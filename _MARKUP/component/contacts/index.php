<div class="c-inner__head u-inner__contacts">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<div class="row">

				<div class="col-sm-12 col-lg-6">
		
					<div class="u-inner__head__style">
											
						<h1>Контакты</h1>

						<p>По всем техническим вопросам и предложениям пишите на почту:</p>

						<a href="mailto:info@veronikastepanova.com" title="">info@veronikastepanova.com</a>

						<p>или воспользуйтесь формой обратной связи ниже</p>
					</div>
				</div>

				<div class="col-sm-12 col-lg-6 u-tar">
						
					<div class="u-width-240">
						<p>Кратко о консультациях. Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem используют потому.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="c-contacts__feedback">
			
			<form class="u-form u-form-contacts" autocomplete="off">
			      				
				<div class="u-form__title">Напишите мне</div>

				<div class="u-form-item">

					<div class="u-select-section">
					
						<label class="u-form-label" for="contacts_theme">Тема обращения *</label>

						<select class="u-input-style u-select-init" id="contacts_theme" name="contacts_theme">
							<option selected="">Хочу консультацию</option>
							<option>Хочу консультацию</option>
							<option>Хочу консультацию</option>
						</select>
					</div>

					<div class="u-text-error"></div>
				</div>

				<div class="row">

					<div class="col-sm-12 col-lg-6">

						<div class="u-form-item">

							<label class="u-form-label" for="contacts_name">Как вас зовут? *</label>
							<input type="text" class="u-input-style" id="contacts_name" name="contacts_name">

							<div class="u-text-error"></div>
						</div>
					</div>

					<div class="col-sm-12 col-lg-6">

						<div class="u-form-item">

							<label class="u-form-label" for="contacts_email">Эл. почта для связи *</label>
							<input type="text" class="u-input-style" id="contacts_email" name="contacts_email" inputmode="email">

							<div class="u-text-error"></div>
						</div>
					</div>
				</div>

				<div class="u-form-item">

					<label class="u-form-label" for="contacts_messags">Сообщение</label>
					<textarea class="u-input-style u-textarea-style" id="contacts_messags" name="contacts_messags"></textarea>

					<div class="u-text-error"></div>
				</div>

				<div class="u-form-item u-form-item-last">

					<button type="submit" class="c-btn u-btn-submit is-bg-yellow">
						<span class="is-submit-title">отправить</span>

						<span class="is-loader">
							<span class="is-loader-circle"></span>
						</span>
					</button>
				</div>
			</form>
		</div>
	</div>
</div>