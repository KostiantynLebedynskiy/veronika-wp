<div class="c-inner__head u-inner__reviews">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
      
			<div class="u-inner__head__style">
									
				<h1>Отзывы</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-reviews u-inner-position">
	
	<div class="container">
		
		<div class="c-reviews__section">
			
			<div class="row u-reviews__row">
				
				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-1.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-vk">
					        <svg aria-hidden="true" width="10" height="7">
			              <use xlink:href="img/svgs.svg#i-vk"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Мария</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>

							<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-3.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-instagram">
					        <svg aria-hidden="true" width="14" height="14">
			              <use xlink:href="img/svgs.svg#i-instagram"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Карина</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-2.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-facebook">
					        <svg aria-hidden="true" width="5" height="11">
			              <use xlink:href="img/svgs.svg#i-facebook"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Алифтина</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке.</p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-4.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-vk">
					        <svg aria-hidden="true" width="10" height="7">
			              <use xlink:href="img/svgs.svg#i-vk"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Александра</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-5.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-vk">
					        <svg aria-hidden="true" width="10" height="7">
			              <use xlink:href="img/svgs.svg#i-vk"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Мария</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>

							<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 u-reviews__item">
					
					<div class="u-reviews__article">
						
						<div class="u-reviews__head">
							
							<div class="u-reviews__photo">
								
								<div class="u-reviews__photo__figure">
					        <img src="img/articles_detail/comment-photo-3.jpg" alt="">
					      </div>

					      <div class="u-reviews__photo__circle is-bg-instagram">
					        <svg aria-hidden="true" width="14" height="14">
			              <use xlink:href="img/svgs.svg#i-instagram"></use>
			            </svg>
					      </div>
							</div>

							<div class="u-reviews__name">Карина</div>
						</div>

						<div class="u-reviews__body">
								
							<p>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке.</p>
						</div>
					</div>
				</div>
			</div>

			<div class="u-reviews__show__more">
				<span>Показать все отзывы</span>
			</div>
		</div>
	</div>
</div>