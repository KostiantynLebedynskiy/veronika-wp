<div class="c-follow">

	<div class="container">
		
		<h2>Дружить</h2>

		<div class="c-follow__photo">
			<img src="img/follow/follow-photo.png" alt="">
		</div>

		<div class="c-follow__section">
			
			<div class="c-follow__section__row">
				
				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-youtube">
						<div class="c-follow__icon">
							<svg aria-hidden="true" width="28" height="22">
	              <use xlink:href="img/svgs.svg#i-youtube"></use>
	            </svg>
						</div>

						<div class="c-follow__name">
							<div class="c-follow__name__link">YouTube</div>
						</div>

						<p>700к подписчиков</p>
					</a>
				</div>

				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-instagram">
						<div class="c-follow__icon">
							<svg aria-hidden="true" width="26" height="26">
	              <use xlink:href="img/svgs.svg#i-instagram"></use>
	            </svg>
						</div>
						
						<div class="c-follow__name">
							<div class="c-follow__name__link">Instagram</div>
						</div>

						<p>@veronik20011</p>
					</a>
				</div>

				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-vk">
						<div class="c-follow__icon">
							<svg aria-hidden="true" width="23" height="15">
	              <use xlink:href="img/svgs.svg#i-vk"></use>
	            </svg>
						</div>
						
						<div class="c-follow__name">
							<div class="c-follow__name__link">Вконтакте</div>
						</div>

						<p>50к подписчиков</p>
					</a>
				</div>

				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-facebook">
						<div class="c-follow__icon">
							<svg aria-hidden="true" width="11" height="23">
	              <use xlink:href="img/svgs.svg#i-facebook"></use>
	            </svg>
						</div>
						
						<div class="c-follow__name">
							<div class="c-follow__name__link">Facebook</div>
						</div>

						<p>15к подписчиков</p>
					</a>
				</div>

				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-ok">
						<div class="c-follow__icon">
							<svg aria-hidden="true" width="15" height="23">
	              <use xlink:href="img/svgs.svg#i-ok"></use>
	            </svg>
						</div>
						
						<div class="c-follow__name">
							<div class="c-follow__name__link">Одноклассники</div>
						</div>

						<p>25к подписчиков</p>
					</a>
				</div>

				<div class="c-follow__section__item">
					
					<a href="#" title="" class="u-follow-link is-link-google">

						<div class="c-follow__icon">
							<svg aria-hidden="true" width="20" height="20">
	              <use xlink:href="img/svgs.svg#i-google"></use>
	            </svg>
						</div>
						
						<div class="c-follow__name">
							<div class="c-follow__name__link">Google</div>
						</div>

						<p>10к подписчиков</p>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>