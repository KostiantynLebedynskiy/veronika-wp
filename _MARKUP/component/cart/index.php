<div class="c-inner__head u-inner__cart">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
      
			<div class="u-inner__head__style">
									
				<h1>Корзина</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-cart u-inner-position">
	
	<div class="container">
		
		<div class="c-cart__section">
			
			<div class="c-cart__item">
				
				<div class="row c-cart__item__row">
					
					<div class="col-sm-12 col-xl-6 c-cart__item__col">
						
						<div class="c-cart__item__article">
							
							<div class="u-table">
										
								<div class="u-table-cell u-order__photo">
									
									<a href="#" title="" class="u-order__photo__link">

										<span class="u-order__photo__section">
											<img src="img/account_history/account-history-2.jpg" alt="">
										</span>
									</a>
								</div>

								<div class="u-table-cell u-order__product">
									
									<a href="#" title="" class="u-order__product__name">Свобода от страха критики. Самореализация</a>
								</div>

								<div class="u-table-cell u-order__type">
									<span class="u-order-link__type">вебинар</span>
								</div>

								<div class="u-table-cell u-order__price">											
									<span class="u-order-link__price">6000 &#8381;</span>
								</div>

								<div class="u-table-cell u-order__remove">											
									<span class="u-order-link__remove">
										<svg aria-hidden="true" width="10" height="10">
			                <use xlink:href="img/svgs.svg#i-remove"></use>
			              </svg>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-xl-6 c-cart__item__col">
						
						<div class="c-cart__item__article">
							
							<div class="u-table">
										
								<div class="u-table-cell u-order__photo">
									
									<a href="#" title="" class="u-order__photo__link">

										<span class="u-order__photo__section">
											<img src="img/account_history/account-history-4.jpg" alt="">
										</span>
									</a>
								</div>

								<div class="u-table-cell u-order__product">
									
									<a href="#" title="" class="u-order__product__name">Как выйти замуж за иностранца и уехать жить заграницу?</a>
								</div>

								<div class="u-table-cell u-order__type">
									<span class="u-order-link__type">Тренинг</span>
								</div>

								<div class="u-table-cell u-order__price">											
									<span class="u-order-link__price">800 &#8381;</span>
								</div>

								<div class="u-table-cell u-order__remove">											
									<span class="u-order-link__remove">
										<svg aria-hidden="true" width="10" height="10">
			                <use xlink:href="img/svgs.svg#i-remove"></use>
			              </svg>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-xl-6 c-cart__item__col">
						
						<div class="c-cart__item__article">
							
							<div class="u-table">
										
								<div class="u-table-cell u-order__photo">
									
									<a href="#" title="" class="u-order__photo__link">

										<span class="u-order__photo__section">
											<img src="img/account_history/account-history-3.jpg" alt="">
										</span>
									</a>
								</div>

								<div class="u-table-cell u-order__product">
									
									<a href="#" title="" class="u-order__product__name">Вся правда, которой вы не знали о нимфоманках</a>
								</div>

								<div class="u-table-cell u-order__type">
									<span class="u-order-link__type">статья</span>
								</div>

								<div class="u-table-cell u-order__price">											
									<span class="u-order-link__price">500 &#8381;</span>
								</div>

								<div class="u-table-cell u-order__remove">											
									<span class="u-order-link__remove">
										<svg aria-hidden="true" width="10" height="10">
			                <use xlink:href="img/svgs.svg#i-remove"></use>
			              </svg>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-xl-6 c-cart__item__col">
						
						<div class="c-cart__item__article">
							
							<div class="u-table">
										
								<div class="u-table-cell u-order__photo">
									
									<a href="#" title="" class="u-order__photo__link">

										<span class="u-order__photo__section">
											<img src="img/account_history/account-history-5.jpg" alt="">
										</span>
									</a>
								</div>

								<div class="u-table-cell u-order__product">
									
									<a href="#" title="" class="u-order__product__name">Кто ты: "Жертва или насильник?"</a>
								</div>

								<div class="u-table-cell u-order__type">
									<span class="u-order-link__type">статья</span>
								</div>

								<div class="u-table-cell u-order__price">											
									<span class="u-order-link__price">500 &#8381;</span>
								</div>

								<div class="u-table-cell u-order__remove">											
									<span class="u-order-link__remove">
										<svg aria-hidden="true" width="10" height="10">
			                <use xlink:href="img/svgs.svg#i-remove"></use>
			              </svg>
									</span>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-xl-6 c-cart__item__col">
						
						<div class="c-cart__item__article">
							
							<div class="u-table">
										
								<div class="u-table-cell u-order__photo">
									
									<a href="#" title="" class="u-order__photo__link">

										<span class="u-order__photo__video"></span>

										<span class="u-order__photo__section">
											<img src="img/account_history/account-history-1.jpg" alt="">
										</span>
									</a>
								</div>

								<div class="u-table-cell u-order__product">
									
									<a href="#" title="" class="u-order__product__name">Создание своего тренинга. Профессия психолог</a>
								</div>

								<div class="u-table-cell u-order__type">
									<span class="u-order-link__type">видео</span>
								</div>

								<div class="u-table-cell u-order__price">											
									<span class="u-order-link__price">4500 &#8381;</span>
								</div>

								<div class="u-table-cell u-order__remove">											
									<span class="u-order-link__remove">
										<svg aria-hidden="true" width="10" height="10">
			                <use xlink:href="img/svgs.svg#i-remove"></use>
			              </svg>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="u-width-500">
			
				<div class="c-cart__result is-promocode2"><!-- /.is-promocode -->

					<div class="c-cart__result__title">Итого</div>

					<p>5 материалов на общую сумму</p>

					<div class="c-cart__result__price">12 300 <i>&#8381;</i> <sup>12 300 &#8381;</sup></div>

					<div class="c-cart__result__form">
						<span class="u-cart-promo__percent">-10%</span>

						<input type="text" class="u-input-style u-input-style-promocode" id="cart_promocode" name="cart_promocode" placeholder="Есть промо-код?" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Есть промо-код?'">
						<div class="u-text-error">Неправильный промо-код</div>

						<span class="u-cart-promo__remove">
							<svg aria-hidden="true" width="10" height="10">
	              <use xlink:href="img/svgs.svg#i-remove"></use>
	            </svg>
						</span>
					</div>
				</div>
			</div>

			<hr>

			<div class="u-width-500">

				<form class="u-form u-form-checkout" autocomplete="off">
					
					<div class="u-section__checkout">

						<div class="c-cart__result__title">Оформить заказ</div>

						<div class="u-form__item">
							
							<ul class="u-radio__test__list u-radio__checkout__list c-cart__tabs__navigator">
								<li>
									<label class="u-radio__test__label is-active" data-tab-link="new_buyer">
										<input type="radio" class="u-radio__test__input" name="cart_checkout" checked="">
										<span class="u-radio__test__section">
											<span class="u-radio__test__box">
												<svg aria-hidden="true" width="13" height="9">
			                    <use xlink:href="img/svgs.svg#i-checked"></use>
			                  </svg>
											</span>
											<span class="u-radio__test__name">Я новый покупатель</span>
										</span>
									</label>
								</li>
								<li>
									<label class="u-radio__test__label" data-tab-link="regular_customer">
										<input type="radio" class="u-radio__test__input" name="cart_checkout">
										<span class="u-radio__test__section">
											<span class="u-radio__test__box">
												<svg aria-hidden="true" width="13" height="9">
			                    <use xlink:href="img/svgs.svg#i-checked"></use>
			                  </svg>
											</span>
											<span class="u-radio__test__name">Я постоянный клиент</span>
										</span>
									</label>
								</li>
							</ul>
						</div>

						<div class="c-cart__tabs__content">
						
							<div data-tab-content="new_buyer" class="c-checkout__tabs__section is-active">

								<div class="u-form__item">

							  	<label class="u-form__label" for="cart_checkout_name">Имя *</label>
							  	<input type="text" class="u-input-style" id="cart_checkout_name" name="cart_checkout_name">
							  	<div class="u-text-error"></div>
							  </div>

							  <div class="u-form__item">

							  	<label class="u-form__label" for="cart_checkout_surname">Фамилия *</label>
							  	<input type="text" class="u-input-style" id="cart_checkout_surname" name="cart_checkout_surname">
							  	<div class="u-text-error"></div>
							  </div>

							  <div class="u-form__item">

							  	<label class="u-form__label" for="cart_checkout_email">Эл. почта *</label>
							  	<input type="text" class="u-input-style" id="cart_checkout_email" name="cart_checkout_email">
							  	<div class="u-text-error"></div>
							  </div>
							</div>

							<div data-tab-content="regular_customer" class="c-checkout__tabs__section">

							  <div class="u-form__item">

							  	<label class="u-form__label" for="cart_checkout_email_regular">Эл. почта *</label>
							  	<input type="text" class="u-input-style" id="cart_checkout_email_regular" name="cart_checkout_email_regular">
							  	<div class="u-text-error"></div>
							  </div>

							  <div class="u-form__item">

							  	<label class="u-form__label" for="cart_checkout_password_regular">Пароль *</label>
							  	<input type="password" class="u-input-style" id="cart_checkout_password_regular" name="cart_checkout_password_regular">
							  	<div class="u-text-error"></div>
							  </div>
							</div>
						</div>
					</div>
					
					<div class="u-section__payment">

				  	<div class="c-cart__result__title">Оплата</div>

				  	<div class="u-form__item">
							
							<ul class="u-radio__test__list u-radio__checkout__list">
								<li>
									<label class="u-radio__test__label">
										<input type="radio" class="u-radio__test__input" name="cart_payment" checked="">
										<span class="u-radio__test__section">
											<span class="u-radio__test__box">
												<svg aria-hidden="true" width="13" height="9">
			                    <use xlink:href="img/svgs.svg#i-checked"></use>
			                  </svg>
											</span>
											<span class="u-radio__test__name">
												<img src="img/cart/paypal.png" alt="">
											</span>
										</span>
									</label>
								</li>
								<li>
									<label class="u-radio__test__label">
										<input type="radio" class="u-radio__test__input" name="cart_payment">
										<span class="u-radio__test__section">
											<span class="u-radio__test__box">
												<svg aria-hidden="true" width="13" height="9">
			                    <use xlink:href="img/svgs.svg#i-checked"></use>
			                  </svg>
											</span>
											<span class="u-radio__test__name">
												<img src="img/cart/robokassa.png" alt="">
											</span>
										</span>
									</label>
								</li>
							</ul>
						</div>

						<div class="u-form__item">
							<p>Оплата через PayPal. Здесь представлены различные<br> способы оплаты. Банковские карты, электронные<br> кошельки, сотовые операторы</p>

							<div class="u-form-agreement">

								<label class="u-checkbox__agreement__label">
									<input type="checkbox" class="u-checkbox__agreement__input" name="cart_agreement">
									<span class="u-checkbox__agreement__section">
										<span class="u-checkbox__agreement__box">
											<svg aria-hidden="true" width="13" height="9">
		                    <use xlink:href="img/svgs.svg#i-checked"></use>
		                  </svg>
										</span>
										<span class="u-checkbox__agreement__name">Выражаю согласие на обработку своих <a href="#" title="">персональных данных</a></span>
									</span>
								</label>
							</div>
						</div>
				  </div>

				  <div class="u-form__item">

						<button type="submit" class="c-btn is-bg-yellow u-btn-checkout">
				  		<span class="is-submit-title">оплатить</span>

							<span class="is-loader">
								<span class="is-loader-circle"></span>
							</span>
				  	</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>