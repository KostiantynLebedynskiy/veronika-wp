<div class="c-articles-detail c-articles-detail-pay">

	<div class="container">

		<div class="c-test-detail_back">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
      
			<a href="test.php" title="" class="u-link-test__back">
				<svg aria-hidden="true" width="7" height="12">
          <path d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z" fill="currentColor"/>
        </svg>
        <span>к списку статей</span>
			</a>
		</div>

		<div class="u-style-box">

			<div class="c-articles-detail__head">
				
				<div class="c-articles-detail__user__photo">
					<img src="img/articles_detail/articles-detail-veronika.png" alt="">
				</div>

				<div class="row u-head__row">
					
					<div class="col-sm-12 col-lg-3 u-head__item u-head__item__date">
						
						<div class="u-articles-detail__date">15 мая 2019</div>
					</div>

					<div class="col-sm-12 col-lg-6 u-head__item u-head__item__tags">
						
						<div class="u-articles-detail__tag">
							
							<ul>
								<li><a href="#" title="">#статьи</a></li>
								<li><a href="#" title="">#Сексология</a></li>
								<li><a href="#" title="">#нимфомания</a></li>
								<li><a href="#" title="">#18+</a></li>
								<li><a href="#" title="">#фрейд</a></li>
							</ul>
						</div>
					</div>

					<div class="col-sm-12 col-lg-3 u-head__item u-head__item__comments">
						
						<div class="u-articles-detail__comments">
							<span class="u-articles-detail__comments__name">Комментарии</span>

							<span class="u-articles-detail__comments__count">
								<svg aria-hidden="true" width="26" height="26">
                  <use xlink:href="img/svgs.svg#i-comment"></use>
                </svg>
								<i>5</i>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="u-style-box__test__head">

				<div class="u-style-box__test__photo" style="background-image: url(img/articles_detail/articles-detail-pay-bg.jpg);">
					<div class="u-section__label is-years">18+</div>
				</div>

				<div class="row align-items-end u-style-box__height">

					<div class="col">

						<div class="c-test-detail__box u-articles-detail-pay__box">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">500 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-head__style__box">
											
								<h1>Равноправие в супружестве,<br> возможно-ли оно?</h1>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="c-articles-detail__section u-style-seo-articles">
				
				<div class="u-wd-760">
					
					<h3>Звонит как-то старинный приятель и просится пожить, пока квартиру не снимет. Говорит, от жены ушел. Лишний диван у нас был, так что пустили. Из любопытства поинтересовались, что случилось.</h3>

					<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset.</p>
				</div>
			</div>

			<div class="c-articles-detail__pdd u-wd-1280">

				<div class="u-articles-detail-pay">

					<svg aria-hidden="true" width="18" height="22" class="u-icon-cart">
            <use xlink:href="img/svgs.svg#i-cart"></use>
          </svg>
					
					<div class="b-h1">Чтобы посмотреть контент полностью,<br> оплатите доступ</div>

					<div class="u-articles-detail-pay__price">500 <span>&#8381;</span></div>

					<a href="#" title="" class="c-btn is-bg-yellow u-link-add-to-cart">добавить в корзину</a>

					<a href="#" title="" class="c-btn is-bg-white u-link-buy-one-click">купить в 1 клик</a>
				</div>

				<div class="c-test-detail_share">
				
					<div class="u-share__title">
						<svg aria-hidden="true" width="14" height="16">
              <use xlink:href="img/svgs.svg#i-share"></use>
            </svg>
            <span>поделиться</span>
					</div>

					<ul class="u-share__list">

						<li>
							<a href="#" title="" class="u-link-facebook">
								
								<svg aria-hidden="true" width="7" height="13">
		              <use xlink:href="img/svgs.svg#i-facebook"></use>
		            </svg>
		            <i>556</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-twitter">
								
								<svg aria-hidden="true" width="13" height="10">
		              <use xlink:href="img/svgs.svg#i-twitter"></use>
		            </svg>
		            <i>113</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-vk">
								
								<svg aria-hidden="true" width="14" height="9">
		              <use xlink:href="img/svgs.svg#i-vk"></use>
		            </svg>
		            <i>947</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-ok">
								
								<svg aria-hidden="true" width="9" height="14">
		              <use xlink:href="img/svgs.svg#i-ok"></use>
		            </svg>
		            <i>130</i>
							</a>
						</li>
					</ul>
				</div>
				
				<div class="row c-test-detail__post">
					
					<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
						
						<a href="#" title="" class="c-test-detail__post__article">

							<div class="c-test-detail__post__flex">

								<div class="c-test-detail__post__arrow">
									<img src="img/svg/i-arrow-slider-left.svg" alt="">
								</div>

								<div class="c-test-detail__post__photo">
									<img src="img/test_detail/post-img-1.jpg" alt="">
								</div>

								<div class="c-test-detail__post__name">Почему из ребенка вырастает<br> маньяк?</div>

								<span class="u-articles-detail__comments__post">
									<svg aria-hidden="true" width="26" height="26">
	                  <use xlink:href="img/svgs.svg#i-comment"></use>
	                </svg>
									<i>12</i>
								</span>
							</div>
						</a>
					</div>

					<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
						
						<a href="#" title="" class="c-test-detail__post__article">

							<div class="c-test-detail__post__flex">

								<span class="u-articles-detail__comments__post">
									<svg aria-hidden="true" width="26" height="26">
	                  <use xlink:href="img/svgs.svg#i-comment"></use>
	                </svg>
									<i>44</i>
								</span>

								<div class="c-test-detail__post__name">Свобода от страха критики.<br> Самореализация</div>

								<div class="c-test-detail__post__photo">
									<img src="img/test_detail/post-img-2.jpg" alt="">
								</div>

								<div class="c-test-detail__post__arrow">
									<img src="img/svg/i-arrow-slider-right.svg" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>