<div class="c-main__video u-section__video">

	<div class="container">
		
		<div class="u-section__head">

			<h2>Свежие видео</h2>

			<a href="#" title="" class="c-main__video__title">с канала <i>youtube</i></a>
		</div>

		<div class="u-section__product">
			
			<div class="u-style-slider">
				
				<div class="u-item">
					
					<a href="#" title="" class="u-section__video__article">
						
						<div class="u-section__video__photo" style="background-image: url(img/video/video-photo-1.jpg);"></div>

						<div class="u-section__video__youtube"></div>
					</a>

					<div class="u-section__video__description">
						<p>Хочется изменять! Что делать?!</p>
					</div>
				</div>

				<div class="u-item">
					
					<a href="#" title="" class="u-section__video__article">
						
						<div class="u-section__video__photo" style="background-image: url(img/video/video-photo-1.jpg);"></div>

						<div class="u-section__video__youtube"></div>
					</a>

					<div class="u-section__video__description">
						<p>Хочется изменять! Что делать?!</p>
					</div>
				</div>

				<div class="u-item">
					
					<a href="#" title="" class="u-section__video__article">
						
						<div class="u-section__video__photo" style="background-image: url(img/video/video-photo-1.jpg);"></div>

						<div class="u-section__video__youtube"></div>
					</a>

					<div class="u-section__video__description">
						<p>Хочется изменять! Что делать?!</p>
					</div>
				</div>

				<div class="u-item">
					
					<a href="#" title="" class="u-section__video__article">
						
						<div class="u-section__video__photo" style="background-image: url(img/video/video-photo-1.jpg);"></div>

						<div class="u-section__video__youtube"></div>
					</a>

					<div class="u-section__video__description">
						<p>Хочется изменять! Что делать?!</p>
					</div>
				</div>

				<div class="u-item">
					
					<a href="#" title="" class="u-section__video__article">
						
						<div class="u-section__video__photo" style="background-image: url(img/video/video-photo-1.jpg);"></div>

						<div class="u-section__video__youtube"></div>
					</a>

					<div class="u-section__video__description">
						<p>Хочется изменять! Что делать?!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>