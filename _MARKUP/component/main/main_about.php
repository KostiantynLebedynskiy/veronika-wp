<div class="c-main__about">

	<div class="c-main__about__skew">

		<div class="container">

			<div class="row align-items-center">
				
				<div class="col-sm-12 col-lg-4">
					
					<div class="b-h1">
						Друзья,<br>
						я — Вероника Степанова</div>
				</div>

				<div class="col-sm-12 col-lg-8">

					<div class="c-main__about__blockquote">

						<div class="row align-items-center">

							<div class="col-sm-12 col-lg-5">
								
								<div class="c-main__about__photo__row">
									<div class="c-main__about__photo">
										<img src="img/main/main-about-photo.jpg" alt="">
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-lg-7">

								<p>Всю свою жизнь я изучаю психологию и медицину; это то, чем я дышу. Являюсь сторонником интегративного подхода в психотерапевтической практике. Но вы меня, скорее всего, знаете по популярно-психологическому каналу на YouTube, который я веду. И который насчитывает уже полмиллиона подписчиков.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>