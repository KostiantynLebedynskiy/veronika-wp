<div class="c-main__events u-section__events">

	<div class="container">
		
		<div class="u-section__head">
			
			<div class="u-section__head__events">				

				<a href="vebinar_past.php" title="" class="u-link-webinars-recording">
					<i></i>
					<span>Вебинары в записи</span>
				</a>

				<h2>Актуальные вебинары</h2>
			</div>
		</div>

		<div class="u-section__product">
			
			<div class="row u-row u-style-slider is-lazy-load">
				
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>13</i> апр
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-1.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Воспитание детей счастливыми<br> и успешными</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">4500 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>27</i> апр
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-2.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Создание своего тренинга.<br> Профессия психолог</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">6000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>15</i> мая
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-3.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Свобода от страха критики.<br> Самореализация</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">8000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>30</i> мая
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-4.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Создание своего тренинга.<br>Профессия психолог</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">6000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>27</i> апр
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-2.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Создание своего тренинга.<br>Профессия психолог</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">6000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>15</i> мая
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-3.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>Свобода от страха критики.<br> Самореализация</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">8000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events" data-mh="events-group">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">
								
								<div class="u-section__product__date">
									<i>30</i> мая
								</div>

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/events/events-photo-4.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>Создание своего тренинга.<br>Профессия психолог</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">6000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>
						</div>
					</div>
				</div>
			</div>

			<a href="#" title="" class="u-section__product__more">
				<span>
					Все вебинары

					<svg aria-hidden="true" width="7" height="12">
	          <path fill="currentColor" d="M6.8,6.6l-5.4,5.2C1.2,11.9,1,12,0.8,12c-0.2,0-0.4-0.1-0.6-0.2c-0.3-0.3-0.3-0.8,0-1.1l4-3.9H4.2C4.4,6.5,4.7,6.2,4.7,6c0-0.2-0.2-0.5-0.5-0.8h0.1l-4-3.9c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l5.4,5.2C7.1,5.8,7.1,6.3,6.8,6.6z"/>
	        </svg>
				</span>
			</a>
		</div>
	</div>
</div>