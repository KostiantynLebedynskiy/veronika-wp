<div class="c-main__book">

	<div class="container">

		<div class="c-main__book__section">

			<div class="row u-row align-items-center">
				
				<div class="col-sm-12 col-lg-7 u-item">
					
					<div class="u-img-book__row">

						<img src="img/main/main-book.png" alt="" class="u-img-book">
						
						<img src="img/main/main-book-title.png" alt="" class="u-img-book-title">
					</div>
				</div>

				<div class="col-sm-12 col-lg-5 u-item">
					
					<div class="c-main__book__title"><i>1650</i> &#8381;</div>

					<p>Давно выяснено, что при оценке дизайна<br> и композиции читаемый текст мешает<br> сосредоточиться</p>

					<a href="#" title="" class="c-btn is-black u-btn-book">купить</a>
				</div>
			</div>
		</div>
	</div>
</div>