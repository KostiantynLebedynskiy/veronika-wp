<?php
	$tags__more = '<div class="u-product__tags__more">

										<div class="u-product__tags__more__link">
											<svg aria-hidden="true" width="10" height="2">
							          <path fill="currentColor" d="M9,2C8.4,2,8,1.6,8,1s0.4-1,1-1c0.6,0,1,0.4,1,1S9.6,2,9,2zM5,2C4.4,2,4,1.6,4,1s0.4-1,1-1s1,0.4,1,1S5.6,2,5,2zM1,2C0.4,2,0,1.6,0,1s0.4-1,1-1s1,0.4,1,1S1.6,2,1,2z"/>
							        </svg>
							      </div>

							      <div class="u-list-hidden">

							      	<ul>											
												<li><a href="#" title="">#Видеоурок</a></li>
												<li><a href="#" title="">#саморазвитие</a></li>
												<li><a href="#" title="">#упражнения</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#саморазвитие</a></li>
											</ul>
										</div>
									</div>';
?>

<div class="u-main__section u-section__video__lessons">

	<div class="container">
		
		<div class="u-section__head">

			<h2>Тренинги и видеоуроки</h2>
		</div>

		<div class="u-section__product">
				
			<div class="row u-row u-style-slider">
				
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-video-lessons" data-mh="video-lessons-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img" style="background-image: url(img/video_lessons/video-lessons-photo-1.jpg);"></div>

								<div class="u-section__video__play"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Танатотерапевтическое<br> упражнение - только сегодня</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">400 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-video-lessons" data-mh="video-lessons-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img" style="background-image: url(img/video_lessons/video-lessons-photo-2.jpg);"></div>

								<div class="u-section__video__play"></div>
							</a>

							<div class="u-section__product__head__section">

								<p>
									<a href="#" title="">Как выйти замуж за иностранца<br> и уехать жить заграницу?</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">5000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-video-lessons" data-mh="video-lessons-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img" style="background-image: url(img/video_lessons/video-lessons-photo-3.jpg);"></div>

								<div class="u-section__video__play"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Воспитание детей счастливыми<br> и успешными</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-free">бесплатно</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-video-lessons" data-mh="video-lessons-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img" style="background-image: url(img/video_lessons/video-lessons-photo-4.jpg);"></div>

								<div class="u-section__video__play"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Создание своего тренинга.<br> Профессия психолог</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">5000 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-video-lessons" data-mh="video-lessons-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img" style="background-image: url(img/video_lessons/video-lessons-photo-1.jpg);"></div>

								<div class="u-section__video__play"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Танатотерапевтическое<br> упражнение - только сегодня</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
								
								<span class="u-btn-product-price">400 &#8381;</span>
								<span class="u-btn-product-name">Купить</span>
							</a>

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>
			</div>

			<a href="#" title="" class="u-section__product__more">
				<span>
					Все тренинги и видеоуроки

					<svg aria-hidden="true" width="7" height="12">
	          <path fill="currentColor" d="M6.8,6.6l-5.4,5.2C1.2,11.9,1,12,0.8,12c-0.2,0-0.4-0.1-0.6-0.2c-0.3-0.3-0.3-0.8,0-1.1l4-3.9H4.2C4.4,6.5,4.7,6.2,4.7,6c0-0.2-0.2-0.5-0.5-0.8h0.1l-4-3.9c-0.3-0.3-0.3-0.8,0-1.1c0.3-0.3,0.8-0.3,1.1,0l5.4,5.2C7.1,5.8,7.1,6.3,6.8,6.6z"/>
	        </svg>
				</span>
			</a>
		</div>
	</div>
</div>