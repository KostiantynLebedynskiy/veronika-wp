<div class="c-main__slider">

	<div class="container">
		
		<div class="u-table">

			<div class="u-table-cell">

				<div class="c-main__slider__row">

		      <?php
		        $url = explode('/', $_SERVER['REQUEST_URI']);
		        if(empty($url[2]) || $url[2] =='index.php' || $url[2] =='main.php') {
		      ?> 
		      <span class="c-logo__mobile">
		        <img src="img/logo.svg" alt="">
		      </span>
		      <?php
		        } else {
		      ?>              
		      <a href="index.php" title="" class="c-logo__mobile">
		        <img src="img/logo.svg" alt="">
		      </a>
		      <?php
		        }
		      ?>
				
					<div class="row align-items-center c-main__slider__top">
						
						<div class="col-sm-12 col-lg-6">
							
							<div class="u-head__style">
								
								<h1>Психологическая<br>
										онлайн-школа<br>
										Вероники Степановой</h1>
							</div>
						</div>

						<div class="col-sm-12 col-lg-6 u-tar">
							
							<div class="u-width-240">
								<p>Клинический психолог, кандидат психологических наук, член Российской Психотерапевтической Ассоциации.</p>
							</div>
						</div>
					</div>

					<div class="c-main__slider__video">
						
						<div class="c-main__slider__video__title">популярное видео</div>

						<a href="#" title="" class="c-main__slider__video__section">

							<div class="c-main__slider__video__name">
								Как решиться<br>
								на перемены?
							</div>
							
							<div class="c-main__slider__video__play"></div>
						</a>
					</div>

					<div class="c-main__slider__down">
						<span>вниз</span>
						<div class="u-style-mouse">
							<div class="u-style-mouse__wheel"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>