<div class="c-account">

	<div class="container">
		
		<div class="c-account__head">

			<h1>Мой аккаунт</h1>
		</div>

		<div class="u-style-box">
			
			<div class="c-account__user">
				
				<div class="c-account__user__photo">
					<img src="img/account/account-user.jpg" alt="">
				</div>

				<div class="c-account__user__link">
					<span class="upload-photo upp">Обновить фото профиля</span>
				</div>

				<input type='file' name="imgInp" class="imgInp" style="display:none;" accept="image/png, image/jpg, image/jpeg">
			</div>

			<div class="u-wd-500">
				
				<form class="u-form u-form-account" autocomplete="off">
					
					<div class="u-form__item">

				  	<label class="u-form__label" for="account_name">Имя</label>
				  	<input type="text" class="u-input-style" id="account_name" name="account_name" value="Мария">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_surname">Фамилия</label>
				  	<input type="text" class="u-input-style" id="account_surname" name="account_surname" value="Фролова">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_email">Эл. почта</label>
				  	<input type="text" class="u-input-style" id="account_email" name="account_email" value="frolova_maria@gmail.com">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_city">Город</label>
				  	<input type="text" class="u-input-style" id="account_city" name="account_city" value="Москва">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__name">Сменить пароль</div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_current_password">Текущий пароль</label>
				  	<input type="password" class="u-input-style" id="account_current_password" name="account_current_password" value="**************">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_new_password">Новый пароль</label>
				  	<input type="password" class="u-input-style" id="account_new_password" name="account_new_password" value="*********">
				  	<div class="u-text-error"></div>
				  </div>

				  <div class="u-form__item">

				  	<label class="u-form__label" for="account_repeat_password">Повторите новый пароль</label>
				  	<input type="password" class="u-input-style" id="account_repeat_password" name="account_repeat_password" value="*********">
				  	<div class="u-text-error"></div>
				  </div>
					
					<div class="u-form__item">

						<button type="submit" class="c-btn is-bg-yellow u-btn-save">
				  		<span class="is-submit-title">сохранить</span>

							<span class="is-loader">
								<span class="is-loader-circle"></span>
							</span>
				  	</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>