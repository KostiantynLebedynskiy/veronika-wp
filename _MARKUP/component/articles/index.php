<?php
	$tags__more__articles = '<div class="u-product__tags__more">

										<div class="u-product__tags__more__link">
											<svg aria-hidden="true" width="10" height="2">
							          <path fill="currentColor" d="M9,2C8.4,2,8,1.6,8,1s0.4-1,1-1c0.6,0,1,0.4,1,1S9.6,2,9,2zM5,2C4.4,2,4,1.6,4,1s0.4-1,1-1s1,0.4,1,1S5.6,2,5,2zM1,2C0.4,2,0,1.6,0,1s0.4-1,1-1s1,0.4,1,1S1.6,2,1,2z"/>
							        </svg>
							      </div>

							      <div class="u-list-hidden">

							      	<ul>
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>
									</div>';
?>

<div class="c-inner__head u-inner__gallery">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<div class="u-inner__head__style">
									
				<h1>Мои статьи</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-gallery u-inner-position">
	
	<div class="container">

		<div class="c-gallery__section">

			<div class="c-gallery__filter">
				
				<div class="row align-items-center c-gallery__filter__row">
					
					<div class="col-sm-12 col-lg-4 u-item-filter u-item__yt">
						
						<ul class="u-articles__filter__list">
							
							<li>
								<label class="u-articles__filter__label">
									<input type="radio" name="articles_filter" class="u-articles__filter__input" checked="">
									<span class="u-articles__filter__section">
										<span class="u-articles__filter__text">Все</span>
									</span>
								</label>
							</li>

							<li>
								<label class="u-articles__filter__label">
									<input type="radio" name="articles_filter" class="u-articles__filter__input">
									<span class="u-articles__filter__section">
										<svg aria-hidden="true" width="17" height="11">
		                  <path fill="currentColor" d="M16.3,3.5c0,0,0,0.1,0,0.1l-1,4C15.2,7.8,15,8,14.8,8L8.5,8c0,0,0,0,0,0H2.2C2,8,1.8,7.9,1.8,7.7l-1-4c0,0,0-0.1,0-0.1C0.3,3.3,0,3,0,2.6c0-0.5,0.4-1,1-1c0.6,0,1,0.4,1,1c0,0.3-0.1,0.6-0.4,0.7l1.3,1.3c0.3,0.3,0.8,0.5,1.3,0.5c0.6,0,1.1-0.3,1.4-0.7l2.2-2.7C7.6,1.5,7.5,1.2,7.5,1c0-0.5,0.4-1,1-1c0.6,0,1,0.4,1,1c0,0.3-0.1,0.5-0.3,0.7c0,0,0,0,0,0l2.1,2.8c0.3,0.4,0.9,0.7,1.4,0.7c0.5,0,0.9-0.2,1.3-0.5l1.3-1.3C15.1,3.1,15,2.9,15,2.6c0-0.5,0.4-1,1-1c0.6,0,1,0.4,1,1C17,3,16.7,3.3,16.3,3.5zM15.2,9.4C15.2,9.2,15,9,14.7,9H2.4C2.1,9,1.9,9.2,1.9,9.4v1.1c0,0.3,0.2,0.5,0.5,0.5h12.3c0.3,0,0.5-0.2,0.5-0.5V9.4z"/>
		                </svg>
										<span class="u-articles__filter__text">Премиум</span>
									</span>
								</label>
							</li>

							<li>
								<label class="u-articles__filter__label">
									<input type="radio" name="articles_filter" class="u-articles__filter__input">
									<span class="u-articles__filter__section">
										<span class="u-articles__filter__text">Бесплатные</span>
									</span>
								</label>
							</li>
						</ul>
					</div>

					<div class="col-6 col-lg-4 u-item-filter u-item__sort">
						
						<!-- <div class="u-gallery__sort">

							<div class="u-gallery__sort__head u-select-section">

								<select class="u-input-style u-select-init" id="gallery_sort" name="gallery_sort">
									<option selected="">Сначала новые</option>
									<option>Сначала новые 2</option>
									<option>Сначала новые 3</option>
								</select>
							</div>
						</div> -->

						<div class="u-gallery__sort">

							<div class="u-gallery__sort__overlay">
							
								<div class="u-gallery__sort__head">

									<div class="u-gallery__sort__link">
										<span class="u-gallery__sort__link__value">Сортировка</span>

										<svg aria-hidden="true" width="8" height="5">
			                <use xlink:href="img/svgs.svg#i-arrow-bottom"></use>
			              </svg>
									</div>
								</div>

								<div class="u-gallery__sort__body">
									
									<div class="u-gallery__sort__scroll u-scroll-tags-style">

										<ul>
											
											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" checked="" value="Сначала новые">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" value="Сначала новые 2">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые 2</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" value="Сначала новые 3">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые 3</span>
													</span>
												</label>
											</li>
										</ul>
									</div>

									<a href="#" title="" class="c-btn is-bg-yellow u-btn-filter-apply u-btn-filter-apply-sort">применить</a>
								</div>
							</div>
						</div>
					</div>

					<div class="col-6 col-lg-4 u-item-filter u-item__tag">
						
						<div class="u-gallery__sort">

							<div class="u-gallery__sort__overlay">
							
								<div class="u-gallery__sort__head">

									<div class="u-gallery__sort__link">
										Выбрать рубрику

										<svg aria-hidden="true" width="8" height="5">
			                <use xlink:href="img/svgs.svg#i-arrow-bottom"></use>
			              </svg>
									</div>

									<div class="u-gallery__sort__remove">
										Рубрики: <i>13</i>
										
										<span class="u-gallery__sort__remove__link">
											<svg aria-hidden="true" width="10" height="10">
				                <use xlink:href="img/svgs.svg#i-remove"></use>
				              </svg>
				            </span>
									</div>
								</div>

								<div class="u-gallery__sort__body">
									
									<div class="u-gallery__sort__scroll u-scroll-tags-style">

										<ul>
											
											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v1" class="u-checkbox__input" checked="">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сексология</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v2" class="u-checkbox__input" checked="">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Отношения</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v3" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v4" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v4" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v6" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v7" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v8" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v9" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v10" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v11" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v12" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v13" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="v14" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>
										</ul>
									</div>

									<a href="#" title="" class="c-btn is-bg-yellow u-btn-filter-apply u-btn-filter-apply-rubrik">применить</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="c-articles__section">

				<div class="row is-lazy-load">

					<div class="col-sm-12 col-lg-12 col-xl-9 u-articles-item">
			
						<div class="row u-row is-lazy-load">

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">
									
											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-1.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>

											<div class="u-section__label is-years">
												18+
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Вся правда, которой вы не знали<br> о нимфоманках</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-2.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Равноправие в супружестве,<br> возможно-ли оно?</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-free">бесплатно</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-3.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Почему из ребенка вырастает<br> маньяк?</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-4.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Кто ты: "Жертва или<br> насильник?"</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-5.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>

											<div class="u-section__label is-years">
												18+
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Вся правда, которой вы не знали<br> о нимфоманках</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-6.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Равноправие в супружестве,<br> возможно-ли оно?</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-2.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>

											<div class="u-section__label is-years">
												18+
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Вся правда, которой вы не знали<br> о нимфоманках</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-3.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Равноправие в супружестве,<br> возможно-ли оно?</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-free">бесплатно</span>
											<span class="u-btn-product-name">Читать</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>

							<div class="col-sm-12 col-md-6 col-lg-4 u-item">
								
								<div class="u-section__product__article is-video-lessons" data-mh="article-group">
									
									<div class="u-section__product__head">

										<a href="#" class="u-section__product__video">

											<div class="u-section__video__img u-lazy" data-imagesrc="img/articles/articles-photo-1.jpg" data-alt="">
												<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
											</div>
										</a>

										<div class="u-section__product__head__section">

											<p>
												<a href="#" title="">Почему из ребенка вырастает<br> маньяк?</a>
											</p>
										</div>
									</div>

									<div class="u-section__product__footer">

										<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
											
											<span class="u-btn-product-price">250 &#8381;</span>
											<span class="u-btn-product-name">Купить</span>
										</a>

										<div class="u-section__product__tags">
											
											<ul>
												
												<li><a href="#" title="">#статьи</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфомания</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#фрейд</a></li>
												<li><a href="#" title="">#сексология</a></li>
											</ul>
										</div>

										<?php echo $tags__more__articles;?>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12 col-lg-12 col-xl-3 u-articles-item">
						
						<div class="c-articles__aside">

							<div class="c-articles__aside__item">
							
								<div class="c-articles__aside__title">Рубрики</div>

								<ul class="u-articles__rubric">

									<li>
										<a href="#" title=""># Сексология <i>69</i></a>										
									</li>

									<li>
										<a href="#" title=""># Отношения <i>33</i></a>										
									</li>

									<li>
										<a href="#" title=""># Психические расстройства <i>40</i></a>					
									</li>

									<li>
										<a href="#" title=""># Дети и подростки <i>62</i></a>
									</li>

									<li>
										<a href="#" title=""># Философия жизни <i>47</i></a>									
									</li>

									<li>
										<a href="#" title=""># 18+ <i>31</i></a>										
									</li>	
								</ul>

								<a href="#" title="" class="u-link-articles-rubric-more">Показать все</a>
							</div>

							<div class="c-articles__aside__item">

								<div class="c-articles__aside__title">Сейчас обсуждают</div>

								<div class="u-articles__discussing">
									
									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-1.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Почему из ребенка вырастает маньяк?</a>
											</div>

											<div class="u-articles__discussing__count">
												<svg aria-hidden="true" width="26" height="26">
		                      <use xlink:href="img/svgs.svg#i-comment"></use>
		                    </svg>
												<i>5</i>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-2.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Свобода от страха критики</a>
											</div>

											<div class="u-articles__discussing__count">
												<svg aria-hidden="true" width="26" height="26">
		                      <use xlink:href="img/svgs.svg#i-comment"></use>
		                    </svg>
												<i>9</i>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-3.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Воспитание детей счастливыми</a>
											</div>

											<div class="u-articles__discussing__count">
												<svg aria-hidden="true" width="26" height="26">
		                      <use xlink:href="img/svgs.svg#i-comment"></use>
		                    </svg>
												<i>8</i>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-4.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Почему из ребенка вырастает маньяк?</a>
											</div>

											<div class="u-articles__discussing__count">
												<svg aria-hidden="true" width="26" height="26">
		                      <use xlink:href="img/svgs.svg#i-comment"></use>
		                    </svg>
												<i>7</i>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-5.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Свобода от страха критики</a>
											</div>

											<div class="u-articles__discussing__count">
												<svg aria-hidden="true" width="26" height="26">
		                      <use xlink:href="img/svgs.svg#i-comment"></use>
		                    </svg>
												<i>9</i>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="c-articles__aside__item">

								<div class="c-articles__aside__title">Вебинары</div>

								<div class="u-articles__webinars">

									<div class="u-articles__webinars__article">
									
										<div class="u-articles__webinars__date">
											<i>27</i> апр
										</div>

										<a href="#" title="" class="u-articles__webinars__img">
											<img src="img/events/events-photo-2.jpg" alt="">
										</a>
									</div>

									<a href="#" title="" class="u-articles__webinars__link">Создание своего тренинга.<br>Профессия психолог</a>
								</div>
							</div>
							
							<div class="c-articles__aside__item">

								<div class="c-articles__aside__title">Видео</div>

								<div class="u-articles__discussing">
									
									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<span class="u-articles__discussing__video__play"></span>
												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-3.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Почему из ребенка вырастает маньяк?</a>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<span class="u-articles__discussing__video__play"></span>
												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-2.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Первая встреча и контракт с клиентом.</a>
											</div>
										</div>
									</div>

									<div class="u-articles__discussing__item">

										<div class="u-articles__discussing__article">

											<div class="u-articles__discussing__photo">

												<span class="u-articles__discussing__video__play"></span>
												<a href="#" title="" class="u-articles__discussing__photo__link">
													<img src="img/articles/post-img-4.jpg" alt="">
												</a>
											</div>

											<div class="u-articles__discussing__name">
												<a href="#" title="" class="u-articles__discussing__link">Создание тренинга. Профессия психолог</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		</div>

			<div class="u-pagiantion">
				
				<ul>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__prev">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z"></path>
			        </svg>
						</a>
					</li>
					<li><span>1</span></li>
					<li><a href="#" title="">2</a></li>
					<li><a href="#" title="">3</a></li>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__next">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M6.8,5.5L1.4,0.2c-0.3-0.3-0.8-0.3-1.1,0c-0.3,0.3-0.3,0.8,0,1.1l4,3.9H4.2C4.4,5.5,4.7,5.8,4.7,6c0,0.2-0.2,0.5-0.5,0.8h0.1l-4,3.9c-0.3,0.3-0.3,0.8,0,1.1C0.4,11.9,0.6,12,0.8,12c0.2,0,0.4-0.1,0.6-0.2l5.4-5.2C7.1,6.3,7.1,5.8,6.8,5.5z"></path>
			        </svg>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>