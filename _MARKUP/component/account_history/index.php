<div class="c-account-history">

	<div class="container">
		
		<div class="c-account__head">

			<h1>История покупок</h1>
		</div>

		<div class="u-style-box">

			<div class="u-wd-1020">
				
				<div class="c-account-history__row">
					
					<div class="c-account-history__item is-open">
						
						<div class="u-history-order__head">
							
							<div class="u-table">
								
								<div class="u-table-cell u-order__id">
									
									<span class="u-order-link__id">Заказ #33258</span>
								</div>

								<div class="u-table-cell u-order__date">
									
									<span class="u-order-link__date">от 30 июня 2019</span>
								</div>

								<div class="u-table-cell u-order__price">
									
									<span class="u-order-link__material">3 материала на</span>
									<span class="u-order-link__price">12 300 &#8381;</span>
								</div>
							</div>

							<span class="u-order-id__icon">
								<svg width="12" height="7">
			            <path fill="currentColor" d="M5.5,6.8L0.2,1.4C0.1,1.2,0,1,0,0.8s0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l3.9,4V4.2C5.5,4.4,5.8,4.7,6,4.7c0.2,0,0.5-0.2,0.8-0.5v0.1l3.9-4c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L6.6,6.8C6.3,7.1,5.8,7.1,5.5,6.8z"/>
			          </svg>
							</span>
						</div>

						<div class="u-history-order__body" style="display: block;">
							
							<div class="u-history-order__body__row">
								
								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__video"></span>

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-1.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Создание своего тренинга. Профессия психолог</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">ВИДЕО</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">4500 &#8381;</span>
										</div>
									</div>
								</div>

								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-2.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Свобода от страха критики. Самореализация</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">вебинар</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">6000 &#8381;</span>
										</div>
									</div>
								</div>

								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-3.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Вся правда, которой вы не знали о нимфоманках</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">статья</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">500 &#8381;</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="c-account-history__item">
						
						<div class="u-history-order__head">
							
							<div class="u-table">
								
								<div class="u-table-cell u-order__id">
									
									<span class="u-order-link__id">Заказ #66502</span>
								</div>

								<div class="u-table-cell u-order__date">
									
									<span class="u-order-link__date">от 19 июня 2019</span>
								</div>

								<div class="u-table-cell u-order__price">
									
									<span class="u-order-link__material">1 материал на</span>
									<span class="u-order-link__price">500 &#8381;</span>
								</div>
							</div>

							<span class="u-order-id__icon">
								<svg width="12" height="7">
			            <path fill="currentColor" d="M5.5,6.8L0.2,1.4C0.1,1.2,0,1,0,0.8s0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l3.9,4V4.2C5.5,4.4,5.8,4.7,6,4.7c0.2,0,0.5-0.2,0.8-0.5v0.1l3.9-4c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L6.6,6.8C6.3,7.1,5.8,7.1,5.5,6.8z"/>
			          </svg>
							</span>
						</div>

						<div class="u-history-order__body">
							
							<div class="u-history-order__body__row">
								
								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__video"></span>

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-1.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Создание своего тренинга. Профессия психолог</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">ВИДЕО</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">500 &#8381;</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="c-account-history__item">
						
						<div class="u-history-order__head">
							
							<div class="u-table">
								
								<div class="u-table-cell u-order__id">
									
									<span class="u-order-link__id">Заказ #44784</span>
								</div>

								<div class="u-table-cell u-order__date">
									
									<span class="u-order-link__date">от 30 июня 2019</span>
								</div>

								<div class="u-table-cell u-order__price">
									
									<span class="u-order-link__material">2 материала на</span>
									<span class="u-order-link__price">3300 &#8381;</span>
								</div>
							</div>

							<span class="u-order-id__icon">
								<svg width="12" height="7">
			            <path fill="currentColor" d="M5.5,6.8L0.2,1.4C0.1,1.2,0,1,0,0.8s0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l3.9,4V4.2C5.5,4.4,5.8,4.7,6,4.7c0.2,0,0.5-0.2,0.8-0.5v0.1l3.9-4c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L6.6,6.8C6.3,7.1,5.8,7.1,5.5,6.8z"/>
			          </svg>
							</span>
						</div>

						<div class="u-history-order__body">
							
							<div class="u-history-order__body__row">

								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-2.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Свобода от страха критики. Самореализация</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">вебинар</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">6000 &#8381;</span>
										</div>
									</div>
								</div>

								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-3.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Вся правда, которой вы не знали о нимфоманках</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">статья</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">500 &#8381;</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="c-account-history__item">
						
						<div class="u-history-order__head">
							
							<div class="u-table">
								
								<div class="u-table-cell u-order__id">
									
									<span class="u-order-link__id">Заказ #66502</span>
								</div>

								<div class="u-table-cell u-order__date">
									
									<span class="u-order-link__date">от 19 июня 2019</span>
								</div>

								<div class="u-table-cell u-order__price">
									
									<span class="u-order-link__material">1 материал на</span>
									<span class="u-order-link__price">500 &#8381;</span>
								</div>
							</div>

							<span class="u-order-id__icon">
								<svg width="12" height="7">
			            <path fill="currentColor" d="M5.5,6.8L0.2,1.4C0.1,1.2,0,1,0,0.8s0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l3.9,4V4.2C5.5,4.4,5.8,4.7,6,4.7c0.2,0,0.5-0.2,0.8-0.5v0.1l3.9-4c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L6.6,6.8C6.3,7.1,5.8,7.1,5.5,6.8z"/>
			          </svg>
							</span>
						</div>

						<div class="u-history-order__body">
							
							<div class="u-history-order__body__row">
								
								<div class="u-history-order__body__item">

									<div class="u-table">
										
										<div class="u-table-cell u-order__photo">
											
											<a href="#" title="" class="u-order__photo__link">

												<span class="u-order__photo__video"></span>

												<span class="u-order__photo__section">
													<img src="img/account_history/account-history-1.jpg" alt="">
												</span>
											</a>
										</div>

										<div class="u-table-cell u-order__product">
											
											<a href="#" title="" class="u-order__product__name">Создание своего тренинга. Профессия психолог</a>
										</div>

										<div class="u-table-cell u-order__type">
											<span class="u-order-link__type">ВИДЕО</span>
										</div>

										<div class="u-table-cell u-order__price">											
											<span class="u-order-link__price">500 &#8381;</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>