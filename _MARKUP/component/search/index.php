<div class="c-search">

	<div class="container">
		
		<div class="c-search__head">

			<h1>Поиск</h1>
		</div>

		<div class="u-style-box">

			<div class="u-wd-760">
				
				<form class="u-form u-form-search" autocomplete="off">

  				<div class="u-form-item">

  					<input type="text" class="u-input-style" id="search" name="search" placeholder="Поиск" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Поиск'">

  					<div class="u-text-error"></div>

  					<button type="submit" class="c-btn u-btn-submit u-btn-search">

  						<span class="is-submit-title">
  							<svg aria-hidden="true" width="20" height="20">
                  <use xlink:href="img/svgs.svg#i-search"></use>
                </svg>
  						</span>

  						<span class="is-loader">
  							<span class="is-loader-circle"></span>
  						</span>
  					</button>
  				</div>
  			</form>
			</div>

			<div class="c-search__section u-wd-1280">

				<div class="c-search__result__row">
				
					<div class="c-search__result__item">
						
						<p><a href="#" title="">Lorem Ipsum - это текст-"рыба"</a>, часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset.</p>
					</div>

					<div class="c-search__result__item">
						
						<p>Давно выяснено, что при оценке дизайна читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное <a href="#" title="">распределение</a> букв и пробелов в абзацах, которое не получается при простой дубликации.</p>
					</div>

					<div class="c-search__result__item">
						
						<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник <a href="#" title="">создал большую коллекцию размеров</a> и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>