<div class="c-test-detail">

	<div class="container">

		<div class="c-test-detail_back">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<a href="test.php" title="" class="u-link-test__back">
				<svg aria-hidden="true" width="7" height="12">
          <path d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z" fill="currentColor"/>
        </svg>
        <span>к списку тестов</span>
			</a>
		</div>

		<div class="u-style-box">

			<div class="u-style-box__test__head">

				<div class="u-style-box__test__photo" style="background-image: url(img/test_detail/test-detail-intro.jpg);">
				</div>

				<div class="row align-items-end u-style-box__height">

					<div class="col">

						<div class="c-test-detail__box">

							<div class="u-head__style__box">
											
								<h1>Способны ли вы добиться<br> успеха в жизни?</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="u-style-box__test__foot">

				<div class="u-wd-740 u-tac">
					
					<p>Возможно, мы просто не умеем правильно спланировать свой рабочий день, не умеем отвлекаться дома от рабочих проблем. Вполне вероятно, что мы просто не умеем отдыхать. Данный тест поможет определить, обладаете ли вы этим весьма ценным и важным для жизни каждого человека умением. Вам следует оценить, насколько подходят лично вам приведенные ниже высказывания, насколько они отражают реальное положение вещей.</p>

					<a href="test_detail_test.php" title="" class="c-btn is-bg-yellow u-link-test-pass-again">пройти тест</a>
				</div>
			</div>
		</div>
	</div>
</div>