<div class="c-test-detail">

	<div class="container">

		<div class="c-test-detail_back">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<a href="test.php" title="" class="u-link-test__back">
				<svg aria-hidden="true" width="7" height="12">
          <path d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z" fill="currentColor"/>
        </svg>
        <span>к списку тестов</span>
			</a>
		</div>

		<div class="u-style-box">

			<div class="u-style-box__test__head">

				<div class="u-style-box__test__photo" style="background-image: url(img/test_detail/test-detail-bg.jpg);"></div>

				<div class="row align-items-end u-style-box__height">

					<div class="col">

						<div class="c-test-detail__box">
							
							<small>тест</small>
							<p>Способны ли вы добиться успеха в жизни?</p>

							<div class="u-head__style__box">
											
								<h1>Вы набрали 7 из 10 баллов</h1>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="u-style-box__test__foot">

				<div class="u-wd-740 u-tac">
					
					<p>У вас имеются все предпосылки, все возможности для того, чтобы добиться успеха в жизни, чем бы вы ни занимались! Вы наверняка упорны в достижении поставленных перед собой целей и готовы преодолевать любые препятствия на этом пути. В вашем характере явно присутствует достаточное равновесие между инстинктом и разумом, между личной и общественной жизнью, между действием и размышлением.</p>

					<a href="test_detail_test.php" title="" class="c-btn is-bg-yellow u-link-test-pass-again">пройти еще раз</a>
				</div>

				<br>
				<br>
				<br>
				<br>
				<br>

				<div class="u-wd-1020">
					
					<hr>

					<div class="c-test-detail_share">
				
					<div class="u-share__title">
						<svg aria-hidden="true" width="14" height="16">
              <use xlink:href="img/svgs.svg#i-share"></use>
            </svg>
            <span>поделиться</span>
					</div>

					<ul class="u-share__list">

						<li>
							<a href="#" title="" class="u-link-facebook">
								
								<svg aria-hidden="true" width="7" height="13">
		              <use xlink:href="img/svgs.svg#i-facebook"></use>
		            </svg>
		            <i>556</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-twitter">
								
								<svg aria-hidden="true" width="13" height="10">
		              <use xlink:href="img/svgs.svg#i-twitter"></use>
		            </svg>
		            <i>113</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-vk">
								
								<svg aria-hidden="true" width="14" height="9">
		              <use xlink:href="img/svgs.svg#i-vk"></use>
		            </svg>
		            <i>947</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-ok">
								
								<svg aria-hidden="true" width="9" height="14">
		              <use xlink:href="img/svgs.svg#i-ok"></use>
		            </svg>
		            <i>130</i>
							</a>
						</li>
					</ul>
				</div>
				</div>

				<div class="u-wd-1280">
					
					<div class="row c-test-detail__post">
						
						<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
							
							<a href="#" title="" class="c-test-detail__post__article">

								<div class="c-test-detail__post__flex">

									<div class="c-test-detail__post__arrow">
										<img src="img/svg/i-arrow-slider-left.svg" alt="">
									</div>

									<div class="c-test-detail__post__photo">
										<img src="img/test_detail/post-img-1.jpg" alt="">
									</div>

									<div class="c-test-detail__post__name">Каков уровень вашей самооценки?</div>
								</div>
							</a>
						</div>

						<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
							
							<a href="#" title="" class="c-test-detail__post__article">

								<div class="c-test-detail__post__flex">

									<div class="c-test-detail__post__name">Целеустремленный ли Вы человек?</div>

									<div class="c-test-detail__post__photo">
										<img src="img/test_detail/post-img-2.jpg" alt="">
									</div>

									<div class="c-test-detail__post__arrow">
										<img src="img/svg/i-arrow-slider-right.svg" alt="">
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>