<div class="c-test-detail c-test-detail__test">

	<div class="container">

		<div class="c-test-detail_back">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
      
			<a href="test.php" title="" class="u-link-test__back">
				<svg aria-hidden="true" width="7" height="12">
          <path d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z" fill="currentColor"/>
        </svg>
        <span>к списку тестов</span>
			</a>
		</div>

		<div class="u-style-box">

			<div class="c-test-detail__box c-test-detail__box__test">

				<div class="c-test-detail__question">
					<span class="u-test-question">вопрос 1 из 10</span>
				</div>

				<div class="u-head__style__box">
								
					<h1>У вас твёрдая походка и крепкое<br> рукопожатие?</h1>
				</div>

				<ul class="u-radio__test__list">
					<li>
						<label class="u-radio__test__label">
							<input type="radio" class="u-radio__test__input" name="test" checked="">
							<span class="u-radio__test__section">
								<span class="u-radio__test__box">
									<svg aria-hidden="true" width="13" height="9">
                    <use xlink:href="img/svgs.svg#i-checked"></use>
                  </svg>
								</span>
								<span class="u-radio__test__name">Да</span>
							</span>
						</label>
					</li>
					<li>
						<label class="u-radio__test__label">
							<input type="radio" class="u-radio__test__input" name="test">
							<span class="u-radio__test__section">
								<span class="u-radio__test__box">
									<svg aria-hidden="true" width="13" height="9">
                    <use xlink:href="img/svgs.svg#i-checked"></use>
                  </svg>
								</span>
								<span class="u-radio__test__name">Нет</span>
							</span>
						</label>
					</li>
				</ul>

				<a href="test_detail_result.php" title="" class="c-btn is-bg-yellow u-link-test-pass-again">Далее</a>
			</div>
		</div>
	</div>
</div>