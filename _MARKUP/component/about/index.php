<div class="c-inner__head u-inner__about">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
		
			<div class="u-inner__head__style">
									
				<h1>Об авторе</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-about u-inner-position">

	<div class="container">

		<div class="c-about__skew">
		
			<div class="c-about__section">

				<div class="c-about__section__head">

					<div class="u-table">

						<div class="u-table-cell">
					
							<h2>Друзья, я — Вероника Степанова.<br>
								Клинический психолог, кандидат психологических наук,<br>
								член Российской Психотерапевтической Ассоциации</h2>
						</div>
					</div>
				</div>

				<div class="row no-gutters">
					
					<div class="col-sm-12 col-lg-6 u-item__about">
						
						<div class="u-figure__about">
							<img src="img/about/about-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="col-sm-12 col-lg-6 u-item__about u-item__about__top">

						<div class="u-table">

							<div class="u-table-cell">
						
								<div class="u-width-405">
									
									<p>Всю свою жизнь я изучаю психологию и медицину; это то, чем я дышу. Являюсь сторонником интегративного подхода в психотерапевтической практике. Но вы меня, скорее всего, знаете по популярно-психологическому каналу на YouTube, который я веду. И который насчитывает уже полмиллиона подписчиков.</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="row no-gutters flex-lg-row-reverse">

					<div class="col-sm-12 col-lg-6 u-item__about u-item__about__bottom">

						<div class="c-about__photo__title">
								<p>Мое основное образование:<br>
								Санкт-Петербургский
								Государственный Университет</p></div>
						
						<div class="u-figure__about">
							<img src="img/about/about-photo-2.jpg" alt="">
						</div>
					</div>

					<div class="col-sm-12 col-lg-6 u-item__about">
						
						<div class="u-table">

							<div class="u-table-cell">
						
								<div class="u-width-405">
									
									<p>Дополнительное образование: НИПНИ им. В.М. Бехтерева, Portland State University, ТЕМЕНОС, Институт Психологического Консультирования, Медицинский центр Бехтерев: наркологическая клиника, РГПУ им. А.И. Герцена, Санкт-Петербургское Государственное Бюджетное Учреждение Здравоохранения «Городская Нарокологическая Больница», Санкт-Петербургский базовый фармацевтический техникум, Санкт-Петербургский Фельдшерский колледж и др.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>