<div class="c-inner__head u-inner__glossary">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
		
			<div class="u-inner__head__style">
									
				<h1>Термины</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-glossary u-inner-position">

	<div class="container">
		
		<div class="c-glossary__section">

			<div class="c-glossary__abc">

				<ul>
					<li><a href="#" title="">а</a></li>
					<li><a href="#" title="">б</a></li>
					<li><a href="#" title="">в</a></li>
					<li><a href="#" title="">г</a></li>
					<li><a href="#" title="">д</a></li>
					<li><a href="#" title="">е</a></li>
					<li><a href="#" title="">ё</a></li>
					<li><a href="#" title="">ж</a></li>
					<li><a href="#" title="">з</a></li>
					<li><a href="#" title="">и</a></li>

					<li><a href="#" title="">й</a></li>
					<li><a href="#" title="">к</a></li>
					<li><a href="#" title="">л</a></li>
					<li><a href="#" title="">м</a></li>
					<li><a href="#" title="">н</a></li>
					<li><a href="#" title="">о</a></li>
					<li><a href="#" title="">п</a></li>
					<li><a href="#" title="">р</a></li>
					<li><a href="#" title="">с</a></li>
					<li><a href="#" title="">т</a></li>

					<li><a href="#" title="">у</a></li>
					<li><a href="#" title="">ф</a></li>
					<li><a href="#" title="">х</a></li>
					<li><a href="#" title="">ц</a></li>
					<li><a href="#" title="">ч</a></li>
					<li><a href="#" title="">ш</a></li>
					<li><a href="#" title="">щ</a></li>
					<li><a href="#" title="">э</a></li>
					<li><a href="#" title="">ю</a></li>
					<li><a href="#" title="">я</a></li>
				</ul>
			</div>

			<div class="c-glossary__all">

				<div class="c-glossary__all__item">
					
					<div class="row">
						
						<div class="col-3 col-md-2">
							
							<div class="c-glossary__all__letter">A</div>
						</div>

						<div class="col-9 col-md-10">
							
							<div class="c-glossary__all__list">

								<ul>
									<li><a href="#" title="" data-effect="mfp-zoom-in" class="tips-item" data-description="Lorem 1">Абсанс</a></li>
									<li><a href="#" title="" data-effect="mfp-zoom-in" class="tips-item" data-description="Lorem 2">Абстиненция</a></li>
									<li><a href="#" title="" data-effect="mfp-zoom-in" class="tips-item" data-description="Lorem 3">Авторитарность</a></li>
									<li><a href="#" title="">Аверсивная психотерапия</a></li>
									<li><a href="#" title="">Агнозия</a></li>
									<li><a href="#" title="">Агрессия</a></li>
									<li><a href="#" title="">Адаптационный синдром</a></li>
									<li><a href="#" title="">Альфа-переживание</a></li>
									<li><a href="#" title="">Акалькулия</a></li>
									<li><a href="#" title="">Акселерация</a></li>
									<li><a href="#" title="">Алекситимия</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="c-glossary__all__item">
					
					<div class="row">
						
						<div class="col-3 col-md-2">
							
							<div class="c-glossary__all__letter">Б</div>
						</div>

						<div class="col-9 col-md-10">
							
							<div class="c-glossary__all__list">

								<ul>
									<li><a href="#" title="">Безусловный рефлекс</a></li>
									<li><a href="#" title="">Бессознательное</a></li>
									<li><a href="#" title="">Бихевиоризм</a></li>
									<li><a href="#" title="">Булимия (нервная)</a></li>
									<li><a href="#" title="">Биопсихизм</a></li>
									<li><a href="#" title="">Биофил</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="c-glossary__all__item">
					
					<div class="row">
						
						<div class="col-3 col-md-2">
							
							<div class="c-glossary__all__letter">В</div>
						</div>

						<div class="col-9 col-md-10">
							
							<div class="c-glossary__all__list">

								<ul>
									<li><a href="#" title="">Вдохновение</a></li>
									<li><a href="#" title="">Воля</a></li>
									<li><a href="#" title="">Восприятие</a></li>
									<li><a href="#" title="">Вуайеризм</a></li>
									<li><a href="#" title="">Высшие функции</a></li>
									<li><a href="#" title="">Вербальный</a></li>
									<li><a href="#" title="">Влечение</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="c-glossary__all__item">
					
					<div class="row">
						
						<div class="col-3 col-md-2">
							
							<div class="c-glossary__all__letter">Г</div>
						</div>

						<div class="col-9 col-md-10">
							
							<div class="c-glossary__all__list">

								<ul>
									<li><a href="#" title="">Галлюцинация</a></li>
									<li><a href="#" title="">Гендерная роль</a></li>
									<li><a href="#" title="">Герменевтика</a></li>
									<li><a href="#" title="">Гиперпатия</a></li>
									<li><a href="#" title="">Гипноз</a></li>
									<li><a href="#" title="">Гомофобия</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="c-glossary__all__item">
					
					<div class="row">
						
						<div class="col-3 col-md-2">
							
							<div class="c-glossary__all__letter">Д</div>
						</div>

						<div class="col-9 col-md-10">
							
							<div class="c-glossary__all__list">

								<ul>
									<li><a href="#" title="">Деменция</a></li>
									<li><a href="#" title="">Депрессия</a></li>
									<li><a href="#" title="">Дистресс</a></li>
									<li><a href="#" title="">Дисграфия</a></li>
									<li><a href="#" title="">Деперсонализация</a></li>
									<li><a href="#" title="">Дереализация</a></li>
									<li><a href="#" title="">Детерминизм</a></li>
									<li><a href="#" title="">Дисфория</a></li>
									<li><a href="#" title="">Доссонанс</a></li>
									<li><a href="#" title="">Деменция</a></li>
									<li><a href="#" title="">Дисграфия</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>