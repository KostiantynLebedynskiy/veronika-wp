<div class="c-404">

	<div class="container">

		<div class="row align-items-center">
			
			<div class="col-sm-12 col-xl-6 u-item">

				<div class="u-width-560">

					<div class="u-head__style">
					
						<h1>404</h1>
						<h2>Страница не найдена</h2>
						<p>Возможно, она была удалена или никогда не существовала.<br>
							Попробуйте перейти на главную страницу сайта<br>
							или воспользуйтесь поиском</p>

						<a href="index.php" title="" class="c-btn is-bg-white u-link-to-home">На главную</a>
					</div>
				</div>
			</div>

			<div class="col-sm-12 col-xl-6 u-item">
				
				<div class="u-width-500">

					<form class="u-form u-form-search" autocomplete="off">

	  				<div class="u-form-item">

	  					<input type="text" class="u-input-style" id="search" name="search" placeholder="Поиск" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Поиск'">

	  					<div class="u-text-error"></div>

	  					<button type="submit" class="c-btn u-btn-submit u-btn-search">

	  						<span class="is-submit-title">
	  							<svg aria-hidden="true" width="20" height="20">
	                  <use xlink:href="img/svgs.svg#i-search"></use>
	                </svg>
	  						</span>

	  						<span class="is-loader">
	  							<span class="is-loader-circle"></span>
	  						</span>
	  					</button>
	  				</div>
	  			</form>
	  		</div>
			</div>
		</div>
	</div>
</div>