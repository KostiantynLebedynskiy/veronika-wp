<div class="c-inner__head u-inner__vebinar">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>
      
			<div class="u-inner__head__style">
									
				<h1>Тесты</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-vebinar u-inner-position">
	
	<div class="container">
		
		<div class="c-vebinar__section">
			
			<div class="row u-row is-lazy-load">
				
				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-1.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Способны ли вы добиться<br> успеха в жизни?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-2.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Каков уровень вашей<br> самооценки?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-3.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Cчастливы ли Вы в семейной<br> жизни?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-4.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Легко ли вам общаться с людьми,<br> а им с вами?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>


				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-5.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Умеете ли вы отдыхать?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-6.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Целеустремленный ли<br> Вы человек?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-7.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Каков уровень вашей<br> самооценки?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-8.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Способны ли вы добиться<br> успеха в жизни?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>


				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-3.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Cчастливы ли Вы в семейной<br> жизни?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-4.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Легко ли вам общаться с людьми,<br> а им с вами?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-1.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Способны ли вы добиться<br> успеха в жизни?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 u-item">
					
					<div class="u-section__product__article is-section-events">
						
						<div class="u-section__product__head">

							<div class="u-section__product__events">

								<a href="#" title="" class="u-section__product__img">
									<span class="u-lazy" data-imagesrc="img/test/test-photo-9.jpg" data-alt="">
										<span class="is-loader">
	      							<span class="is-loader-circle"></span>
	      						</span>
									</span>
								</a>
							</div>

							<p>
								<a href="#" title="">Каков уровень вашей<br> самооценки?</a>
							</p>
						</div>

						<div class="u-section__product__footer">

							<a href="#" title="" class="c-btn is-yellow u-btn-product-test">
								<span class="u-btn-pass-test">пройти тест</span>
							</a>
						</div>
					</div>
				</div>


			</div>

			<div class="u-pagiantion">
				
				<ul>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__prev">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z"></path>
			        </svg>
						</a>
					</li>
					<li><span>1</span></li>
					<li><a href="#" title="">2</a></li>
					<li><a href="#" title="">3</a></li>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__next">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M6.8,5.5L1.4,0.2c-0.3-0.3-0.8-0.3-1.1,0c-0.3,0.3-0.3,0.8,0,1.1l4,3.9H4.2C4.4,5.5,4.7,5.8,4.7,6c0,0.2-0.2,0.5-0.5,0.8h0.1l-4,3.9c-0.3,0.3-0.3,0.8,0,1.1C0.4,11.9,0.6,12,0.8,12c0.2,0,0.4-0.1,0.6-0.2l5.4-5.2C7.1,6.3,7.1,5.8,6.8,5.5z"></path>
			        </svg>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>