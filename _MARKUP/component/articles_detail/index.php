<div class="c-articles-detail">

	<div class="container">

		<div class="c-test-detail_back">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<a href="test.php" title="" class="u-link-test__back">
				<svg aria-hidden="true" width="7" height="12">
          <path d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z" fill="currentColor"/>
        </svg>
        <span>к списку статей</span>
			</a>
		</div>

		<div class="u-style-box">

			<div class="c-articles-detail__head">
				
				<div class="c-articles-detail__user__photo">
					<img src="img/follow/follow-photo.png" alt="">
				</div>

				<div class="row u-head__row">
					
					<div class="col-sm-12 col-lg-3 u-head__item u-head__item__date">
						
						<div class="u-articles-detail__date">15 мая 2019</div>
					</div>

					<div class="col-sm-12 col-lg-6 u-head__item u-head__item__tags">
						
						<div class="u-articles-detail__tag">
							
							<ul>
								<li><a href="#" title="">#статьи</a></li>
								<li><a href="#" title="">#Сексология</a></li>
								<li><a href="#" title="">#нимфомания</a></li>
								<li><a href="#" title="">#18+</a></li>
								<li><a href="#" title="">#фрейд</a></li>
							</ul>
						</div>
					</div>

					<div class="col-sm-12 col-lg-3 u-head__item u-head__item__comments">
						
						<div class="u-articles-detail__comments">
							<span class="u-articles-detail__comments__name">Комментарии</span>

							<span class="u-articles-detail__comments__count">
								<svg aria-hidden="true" width="26" height="26">
                  <use xlink:href="img/svgs.svg#i-comment"></use>
                </svg>
								<i>5</i>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="u-style-box__test__head">

				<div class="u-style-box__test__photo" style="background-image: url(img/articles_detail/articles-detail-pay-bg.jpg);">
					<div class="u-section__label is-years">18+</div>
				</div>

				<div class="row align-items-end u-style-box__height">

					<div class="col">

						<div class="c-test-detail__box">

							<div class="u-head__style__box">
											
								<h1>Вся правда, которой вы не знали<br> о нимфоманках</h1>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="c-articles-detail__section u-style-seo-articles">
				
				<div class="u-wd-760">
					
					<h3>Звонит как-то старинный приятель и просится пожить, пока квартиру не снимет. Говорит, от жены ушел. Лишний диван у нас был, так что пустили. Из любопытства поинтересовались, что случилось.</h3>

					<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset.</p>

					<h2>Некуда бежать</h2>

					<p>Давно выяснено, что при оценке дизайна читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации.</p>
				</div>
			</div>

			<div class="c-articles-detail__gallery">
				
				<div class="u-style-slider">
					
					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>

					<div class="u-style-slider__item">
						<div class="u-style-slider__article">
							<img src="img/articles_detail/gallery-photo-1.jpg" alt="">
						</div>
					</div>
				</div>
			</div>

			<div class="c-articles-detail__section u-style-seo-articles">
				
				<div class="u-wd-760">

					<p>Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам "lorem ipsum" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий.</p>

					<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона.</p>

					<blockquote>
						<div class="row">
							<div class="col-sm-12 col-md-7 col-xl-9">
								<p>Гиперсексуальность — приятное состояние. Женщина получает яркие многопиковые оргазмы, иной раз они длятся целый час. Она всегда открыта для объятий. Хотя встречаются проблемные случаи: у меня была пациентка, которая не могла носить тесные джинсы — промежностью ощущала шов, и это ее возбуждало.</p>
							</div>

							<div class="col-sm-12 col-md-5 col-xl-3">
								
								<div class="c-articles-detail__author">
									
									<div class="c-articles-detail__author__photo">
										<img src="img/articles_detail/articles-detail-veronika.png" alt="">
									</div>

									<div class="c-articles-detail__author__name">Отвечает Вероника</div>
								</div>
							</div>
						</div>
					</blockquote>
				</div>
			</div>

			<div class="c-articles-detail__article">
				
				<div class="u-wd-1150">
					
					<div class="b-h2">На эту же тему</div>

					<?php
						$tags__more__articles = '<div class="u-product__tags__more">

															<div class="u-product__tags__more__link">
																<svg aria-hidden="true" width="10" height="2">
												          <path fill="currentColor" d="M9,2C8.4,2,8,1.6,8,1s0.4-1,1-1c0.6,0,1,0.4,1,1S9.6,2,9,2zM5,2C4.4,2,4,1.6,4,1s0.4-1,1-1s1,0.4,1,1S5.6,2,5,2zM1,2C0.4,2,0,1.6,0,1s0.4-1,1-1s1,0.4,1,1S1.6,2,1,2z"/>
												        </svg>
												      </div>

												      <div class="u-list-hidden">

												      	<ul>
																	<li><a href="#" title="">#статьи</a></li>
																	<li><a href="#" title="">#Сексология</a></li>
																	<li><a href="#" title="">#нимфомания</a></li>
																	<li><a href="#" title="">#18+</a></li>
																	<li><a href="#" title="">#фрейд</a></li>
																	<li><a href="#" title="">#сексология</a></li>
																</ul>
															</div>
														</div>';
					?>

					<div class="row u-row">
					
						<div class="col-sm-12 col-md-6 col-lg-4 u-item">
							
							<div class="u-section__product__article is-video-lessons" data-mh="article-group">
								
								<div class="u-section__product__head">

									<a href="#" class="u-section__product__video">

										<div class="u-section__video__img" style="background-image: url(img/articles/articles-photo-4.jpg);"></div>
									</a>

									<div class="u-section__product__head__section">

										<p>
											<a href="#" title="">Кто ты: "Жертва или<br> насильник?"</a>
										</p>
									</div>
								</div>

								<div class="u-section__product__footer">

									<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
										
										<span class="u-btn-product-price">250 &#8381;</span>
										<span class="u-btn-product-name">Купить</span>
									</a>

									<div class="u-section__product__tags">
										
										<ul>
											
											<li><a href="#" title="">#статьи</a></li>
											<li><a href="#" title="">#Сексология</a></li>
											<li><a href="#" title="">#нимфомания</a></li>
											<li><a href="#" title="">#18+</a></li>
											<li><a href="#" title="">#фрейд</a></li>
											<li><a href="#" title="">#сексология</a></li>
										</ul>
									</div>

									<?php echo $tags__more__articles;?>
								</div>
							</div>
						</div>

						<div class="col-sm-12 col-md-6 col-lg-4 u-item">
							
							<div class="u-section__product__article is-video-lessons" data-mh="article-group">
								
								<div class="u-section__product__head">

									<a href="#" class="u-section__product__video">

										<div class="u-section__video__img" style="background-image: url(img/articles/articles-photo-5.jpg);"></div>

										<div class="u-section__label is-years">
											18+
										</div>
									</a>

									<div class="u-section__product__head__section">

										<p>
											<a href="#" title="">Вся правда, которой вы не знали<br> о нимфоманках</a>
										</p>
									</div>
								</div>

								<div class="u-section__product__footer">

									<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
										
										<span class="u-btn-product-price">250 &#8381;</span>
										<span class="u-btn-product-name">Купить</span>
									</a>

									<div class="u-section__product__tags">
										
										<ul>
											
											<li><a href="#" title="">#статьи</a></li>
											<li><a href="#" title="">#Сексология</a></li>
											<li><a href="#" title="">#нимфомания</a></li>
											<li><a href="#" title="">#18+</a></li>
											<li><a href="#" title="">#фрейд</a></li>
											<li><a href="#" title="">#сексология</a></li>
										</ul>
									</div>

									<?php echo $tags__more__articles;?>
								</div>
							</div>
						</div>

						<div class="col-sm-12 col-md-6 col-lg-4 u-item">
							
							<div class="u-section__product__article is-video-lessons" data-mh="article-group">
								
								<div class="u-section__product__head">

									<a href="#" class="u-section__product__video">

										<div class="u-section__video__img" style="background-image: url(img/articles/articles-photo-6.jpg);"></div>
									</a>

									<div class="u-section__product__head__section">

										<p>
											<a href="#" title="">Равноправие в супружестве,<br> возможно-ли оно?</a>
										</p>
									</div>
								</div>

								<div class="u-section__product__footer">

									<a href="#" title="" class="c-btn is-yellow u-btn-product-buy">
										
										<span class="u-btn-product-free">бесплатно</span>
										<span class="u-btn-product-name">Купить</span>
									</a>

									<div class="u-section__product__tags">
										
										<ul>
											
											<li><a href="#" title="">#статьи</a></li>
											<li><a href="#" title="">#Сексология</a></li>
											<li><a href="#" title="">#нимфомания</a></li>
											<li><a href="#" title="">#18+</a></li>
											<li><a href="#" title="">#фрейд</a></li>
											<li><a href="#" title="">#сексология</a></li>
										</ul>
									</div>

									<?php echo $tags__more__articles;?>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>

			<div class="c-articles-detail__section u-style-seo-articles">
				
				<div class="u-wd-760">

					<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца.</p>

					<ul>
						<li>Так что же это — распущенность, за которую должно быть стыдно, или все-таки медицинская проблема?</li>
						<li>Словари дают определение нимфомании как гиперсексуальности. </li>
						<li>Но между этими понятиями огромная дистанция.</li>
						<li>Яркий пример — Саманта из «Секса в большом городе».</li>
					</ul>

					<h2>Заполнить пустоту</h2>

					<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений.</p>

					<ol>
						<li>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.</li>
						<li>Lorem Ipsum является стандартной "рыбой" для текстов с начала XVI века.</li>
						<li>Lorem Ipsum - это текст-"рыба", часто используемый.</li>
					</ol>

					<fieldset>
				   	<legend>Прикинь!</legend>
				   	<p>Скульптор Джейми Маккарти составил коллекцию из 400 слепков<br> женских гениталий. Она наглядно показывает, что орган этот<br> индивидуален и едва ли найдутся два похожих.</p>
				  </fieldset>
				</div>
			</div>

			<div class="c-articles-detail__share u-wd-1280">

				<div class="c-test-detail_share">
				
					<div class="u-share__title">
						<svg aria-hidden="true" width="14" height="16">
              <use xlink:href="img/svgs.svg#i-share"></use>
            </svg>
            <span>поделиться</span>
					</div>

					<ul class="u-share__list">

						<li>
							<a href="#" title="" class="u-link-facebook">
								
								<svg aria-hidden="true" width="7" height="13">
		              <use xlink:href="img/svgs.svg#i-facebook"></use>
		            </svg>
		            <i>556</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-twitter">
								
								<svg aria-hidden="true" width="13" height="10">
		              <use xlink:href="img/svgs.svg#i-twitter"></use>
		            </svg>
		            <i>113</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-vk">
								
								<svg aria-hidden="true" width="14" height="9">
		              <use xlink:href="img/svgs.svg#i-vk"></use>
		            </svg>
		            <i>947</i>
							</a>
						</li>

						<li>
							<a href="#" title="" class="u-link-ok">
								
								<svg aria-hidden="true" width="9" height="14">
		              <use xlink:href="img/svgs.svg#i-ok"></use>
		            </svg>
		            <i>130</i>
							</a>
						</li>
					</ul>
				</div>
				
				<div class="row c-test-detail__post">
					
					<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
						
						<a href="#" title="" class="c-test-detail__post__article">

							<div class="c-test-detail__post__flex">

								<div class="c-test-detail__post__arrow">
									<img src="img/svg/i-arrow-slider-left.svg" alt="">
								</div>

								<div class="c-test-detail__post__photo">
									<img src="img/test_detail/post-img-1.jpg" alt="">
								</div>

								<div class="c-test-detail__post__name">Почему из ребенка вырастает<br> маньяк?</div>

								<span class="u-articles-detail__comments__post">
									<svg aria-hidden="true" width="26" height="26">
	                  <use xlink:href="img/svgs.svg#i-comment"></use>
	                </svg>
									<i>12</i>
								</span>
							</div>
						</a>
					</div>

					<div class="col-sm-12 col-lg-6 c-test-detail__post__item">
						
						<a href="#" title="" class="c-test-detail__post__article">

							<div class="c-test-detail__post__flex">

								<span class="u-articles-detail__comments__post">
									<svg aria-hidden="true" width="26" height="26">
	                  <use xlink:href="img/svgs.svg#i-comment"></use>
	                </svg>
									<i>44</i>
								</span>

								<div class="c-test-detail__post__name">Равноправие в супружестве,<br> возможно-ли оно?</div>

								<div class="c-test-detail__post__photo">
									<img src="img/test_detail/post-img-2.jpg" alt="">
								</div>

								<div class="c-test-detail__post__arrow">
									<img src="img/svg/i-arrow-slider-right.svg" alt="">
								</div>
							</div>
						</a>
					</div>
				</div>
			</div>

			<div class="c-articles-detail__comments">

				<div class="u-wd-1020">
				
					<div class="u-section__comments__head">

						<div class="row">
							
							<div class="col-sm-12 col-md-6">
								
								<div class="u-section__comments__title">Комментарии <i>5</i></div>
							</div>

							<div class="col-sm-12 col-md-6">
								
								<div class="u-section__comments__sort">
									
									<ul>
										<li>
											<label class="u-comments__sort__label">
												<input type="radio" class="u-comments__sort__input" name="comments__sort" checked="">
												<span class="u-comments__sort__section">
													<span class="u-comments__sort__name">По рейтингу</span>
												</span>
											</label>
										</li>
										<li>
											<label class="u-comments__sort__label">
												<input type="radio" class="u-comments__sort__input" name="comments__sort">
												<span class="u-comments__sort__section">
													<span class="u-comments__sort__name">Сначала новые</span>
												</span>
											</label>
										</li>
									</ul>
								</div>
							</div>
						</div>					
					</div>

					<div class="u-section__comments__body">
						
						<div class="u-section__comments__row">
							
							<div class="u-section__comments__item">
								
								<div class="u-section__comments__artcle">
									
									<div class="u-section__comments__photo">
										<img src="img/articles_detail/comment-photo-1.jpg" alt="">										
									</div>

									<div class="u-section__comments__section">

										<div class="u-section__comments__name">Мария Фролова</div>

										<div class="u-section__comments__date">3 дня назад</div>

										<p>Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений))</p>

										<div class="u-section__comments__detail">
											
											<span class="u-section__comments__like">
												<svg aria-hidden="true" width="16" height="17">
				                  <use xlink:href="img/svgs.svg#i-like"></use>
				                </svg>
				                <i>89</i>
											</span>

											<span class="u-section__comments__reply">
												<svg aria-hidden="true" width="18" height="13">
				                  <use xlink:href="img/svgs.svg#i-reply"></use>
				                </svg>
				                <i>Ответить</i>
											</span>
										</div>
									</div>
								</div>
							</div>

							<div class="u-section__comments__item">
								
								<div class="u-section__comments__artcle">
									
									<div class="u-section__comments__photo">
										<img src="img/articles_detail/comment-photo-2.jpg" alt="">										
									</div>

									<div class="u-section__comments__section">

										<div class="u-section__comments__name">Ольга Мирошенкова</div>

										<div class="u-section__comments__date">1 день назад</div>

										<p>Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала века</p>

										<div class="u-section__comments__detail">
											
											<span class="u-section__comments__like">
												<svg aria-hidden="true" width="16" height="17">
				                  <use xlink:href="img/svgs.svg#i-like"></use>
				                </svg>
				                <i>77</i>
											</span>

											<span class="u-section__comments__reply">
												<svg aria-hidden="true" width="18" height="13">
				                  <use xlink:href="img/svgs.svg#i-reply"></use>
				                </svg>
				                <i>Ответить</i>
											</span>
										</div>
									</div>
								</div>
							</div>

							<div class="u-section__comments__item">
								
								<div class="u-section__comments__artcle">
									
									<div class="u-section__comments__photo">
										<img src="img/articles_detail/comment-photo-3.jpg" alt="">										
									</div>

									<div class="u-section__comments__section">

										<div class="u-section__comments__name">Карина Герасимчук</div>

										<div class="u-section__comments__date">5 Дней назад</div>

										<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова</p>

										<div class="u-section__comments__detail">
											
											<span class="u-section__comments__like">
												<svg aria-hidden="true" width="16" height="17">
				                  <use xlink:href="img/svgs.svg#i-like"></use>
				                </svg>
				                <i>77</i>
											</span>

											<span class="u-section__comments__reply">
												<svg aria-hidden="true" width="18" height="13">
				                  <use xlink:href="img/svgs.svg#i-reply"></use>
				                </svg>
				                <i>Ответить</i>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="u-section__comments__show__more">
							<span>Показать все комментарии</span>
						</div>

						<div class="u-section__comments__write">
							
							<form class="u-form u-form-comments">
								
								<div class="u-form__item">
							  	<textarea class="u-input-style u-textarea-style" id="comments" name="comments" placeholder="Ваш комментарий" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваш комментарий'"></textarea>
							  	<div class="u-text-error"></div>
							  </div>

							  <div class="u-form__item">

									<button type="submit" class="c-btn is-yellow u-btn-comments">
							  		<span class="is-submit-title">отправить</span>

										<span class="is-loader">
											<span class="is-loader-circle"></span>
										</span>
							  	</button>
								</div>
							</form>
						</div>

						<div class="u-section__comments__login">
							
							<a href="#" title="" class="u-section__comments__login__link">
								<svg aria-hidden="true" width="19" height="20">
                  <use xlink:href="img/svgs.svg#i-login"></use>
                </svg>
								<span>Авторизуйтесь</span>
							</a>

							<p>чтобы оставлять комментарии</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>