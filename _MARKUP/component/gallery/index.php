<?php
	$tags__more = '<div class="u-product__tags__more">

										<div class="u-product__tags__more__link">
											<svg aria-hidden="true" width="10" height="2">
							          <path fill="currentColor" d="M9,2C8.4,2,8,1.6,8,1s0.4-1,1-1c0.6,0,1,0.4,1,1S9.6,2,9,2zM5,2C4.4,2,4,1.6,4,1s0.4-1,1-1s1,0.4,1,1S5.6,2,5,2zM1,2C0.4,2,0,1.6,0,1s0.4-1,1-1s1,0.4,1,1S1.6,2,1,2z"/>
							        </svg>
							      </div>

							      <div class="u-list-hidden">

							      	<ul>											
												<li><a href="#" title="">#youtube</a></li>
												<li><a href="#" title="">#Сексология</a></li>
												<li><a href="#" title="">#нимфоманка</a></li>
												<li><a href="#" title="">#возбудимость</a></li>
												<li><a href="#" title="">#18+</a></li>
												<li><a href="#" title="">#саморазвитие</a></li>
											</ul>
										</div>
									</div>';
?>

<div class="c-inner__head u-inner__gallery">

	<div class="container">

		<div class="c-inner__head__section">

			<a href="index.php" title="" class="c-logo__mobile">
        <img src="img/logo.svg" alt="">
      </a>

			<div class="u-inner__head__style">
									
				<h1>Видеогалерея</h1>
			</div>
		</div>
	</div>
</div>

<div class="c-gallery u-inner-position">
	
	<div class="container">

		<div class="c-gallery__section">

			<div class="c-gallery__filter">
				
				<div class="row align-items-center c-gallery__filter__row">
					
					<div class="col-sm-12 col-lg-4 u-item-filter u-item__yt">
						
						<a href="#" title="" class="u-gallery__link">
							<span class="u-gallery__yt__icon"></span>
							<span class="u-gallery__yt__text">перейти на канал <i>youtube</i></span>
						</a>
					</div>

					<div class="col-6 col-lg-4 u-item-filter u-item__sort">
						
						<!-- <div class="u-gallery__sort">

							<div class="u-gallery__sort__head u-select-section">

								<select class="u-input-style u-select-init" id="gallery_sort" name="gallery_sort">
									<option selected="">Сначала новые</option>
									<option>Сначала новые 2</option>
									<option>Сначала новые 3</option>
								</select>
							</div>
						</div> -->

						<div class="u-gallery__sort">

							<div class="u-gallery__sort__overlay">
							
								<div class="u-gallery__sort__head">

									<div class="u-gallery__sort__link">
										<span class="u-gallery__sort__link__value">Сортировка</span>

										<svg aria-hidden="true" width="8" height="5">
			                <use xlink:href="img/svgs.svg#i-arrow-bottom"></use>
			              </svg>
									</div>
								</div>

								<div class="u-gallery__sort__body">
									
									<div class="u-gallery__sort__scroll u-scroll-tags-style">

										<ul>
											
											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" checked="" value="Сначала новые">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" value="Сначала новые 2">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые 2</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label u-radio__label">
													<input type="radio" name="gallery_sort" class="u-checkbox__input u-checkbox__input__sort" value="Сначала новые 3">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сначала новые 3</span>
													</span>
												</label>
											</li>
										</ul>
									</div>

									<a href="#" title="" class="c-btn is-bg-yellow u-btn-filter-apply u-btn-filter-apply-sort">применить</a>
								</div>
							</div>
						</div>
					</div>

					<div class="col-6 col-lg-4 u-item-filter u-item__tag">
						
						<div class="u-gallery__sort">

							<div class="u-gallery__sort__overlay">
							
								<div class="u-gallery__sort__head">

									<div class="u-gallery__sort__link">
										Выбрать рубрику

										<svg aria-hidden="true" width="8" height="5">
			                <use xlink:href="img/svgs.svg#i-arrow-bottom"></use>
			              </svg>
									</div>

									<div class="u-gallery__sort__remove">
										Рубрики: <i>13</i>
										
										<span class="u-gallery__sort__remove__link">
											<svg aria-hidden="true" width="10" height="10">
				                <use xlink:href="img/svgs.svg#i-remove"></use>
				              </svg>
				            </span>
									</div>
								</div>

								<div class="u-gallery__sort__body">
									
									<div class="u-gallery__sort__scroll u-scroll-tags-style">

										<ul>
											
											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input" checked="">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Сексология</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input" checked="">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Отношения</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Психические расстройства</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Дети и подростки</span>
													</span>
												</label>
											</li>

											<li>
												<label class="u-checkbox__label">
													<input type="checkbox" name="" class="u-checkbox__input">
													<span class="u-checkbox__section">														
														<span class="u-checkbox__box">
															<svg aria-hidden="true" width="9" height="7">
					                      <use xlink:href="img/svgs.svg#i-checked"></use>
					                    </svg>
														</span>
														<span class="u-checkbox__description">Возбуждение</span>
													</span>
												</label>
											</li>
										</ul>
									</div>

									<a href="#" title="" class="c-btn is-bg-yellow u-btn-filter-apply">применить</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row u-row is-lazy-load">

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-1.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">

								<p>
									<a href="#" title="">Синдром нервной повышенной<br> возбудимости</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-2.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как понять что это твой<br> мужчина?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-3.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">

								<p>
									<a href="#" title="">Кунилингус. Как заставить его лизать?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-4.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как правильно  вести свою<br> социальную страницу?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-5.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Друзья моего мужчины!<br> Осторожно!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-6.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как научиться радоваться<br> праздникам!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-1.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Синдром нервной повышенной<br> возбудимости</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-2.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как понять что это твой<br> мужчина?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-3.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Кунилингус. Как заставить его лизать?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-4.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как правильно  вести свою<br> социальную страницу?!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-5.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>
							
							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Друзья моего мужчины!<br> Осторожно!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>

				<div class="col-sm-12 col-md-6 col-lg-4 u-item">
					
					<div class="u-section__product__article is-video-gallery" data-mh="video-group">
						
						<div class="u-section__product__head">

							<a href="#" class="u-section__product__video">

								<div class="u-section__video__img u-section__gallery__video__img">
									<img src="img/gallery/gallery-photo-6.jpg" alt="">
								</div>

								<div class="u-section__video__youtube"></div>
							</a>

							<div class="u-section__product__head__section">
								<p>
									<a href="#" title="">Как научиться радоваться<br> праздникам!</a>
								</p>
							</div>
						</div>

						<div class="u-section__product__footer">

							<div class="u-section__product__tags">
								
								<ul>
									
									<li><a href="#" title="">#Видеоурок</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
									<li><a href="#" title="">#упражнения</a></li>
									<li><a href="#" title="">#18+</a></li>
									<li><a href="#" title="">#фрейд</a></li>
									<li><a href="#" title="">#саморазвитие</a></li>
								</ul>
							</div>

							<?php echo $tags__more;?>
						</div>
					</div>
				</div>
			</div>

			<div class="u-pagiantion">
				
				<ul>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__prev">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M0.2,6.6l5.4,5.2C5.8,11.9,6,12,6.2,12c0.2,0,0.4-0.1,0.6-0.2c0.3-0.3,0.3-0.8,0-1.1l-4-3.9h0.1C2.6,6.5,2.3,6.2,2.3,6c0-0.2,0.2-0.5,0.5-0.8H2.7l4-3.9c0.3-0.3,0.3-0.8,0-1.1C6.5-0.1,6-0.1,5.6,0.2L0.2,5.5C-0.1,5.8-0.1,6.3,0.2,6.6z"></path>
			        </svg>
						</a>
					</li>
					<li><span>1</span></li>
					<li><a href="#" title="">2</a></li>
					<li><a href="#" title="">3</a></li>
					<li>
						<a href="#" title="" class="u-page-numbers u-page__next">
							<svg aria-hidden="true" width="7" height="12">
			          <path fill="currentColor" d="M6.8,5.5L1.4,0.2c-0.3-0.3-0.8-0.3-1.1,0c-0.3,0.3-0.3,0.8,0,1.1l4,3.9H4.2C4.4,5.5,4.7,5.8,4.7,6c0,0.2-0.2,0.5-0.5,0.8h0.1l-4,3.9c-0.3,0.3-0.3,0.8,0,1.1C0.4,11.9,0.6,12,0.8,12c0.2,0,0.4-0.1,0.6-0.2l5.4-5.2C7.1,6.3,7.1,5.8,6.8,5.5z"></path>
			        </svg>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>