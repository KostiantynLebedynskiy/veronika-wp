<div class="b-popup b-popup-add-to-cart mfp-with-anim">

  <div class="u-form__head">

    <div class="u-form__logo">
      
      <div class="u-form__logo__figure">

        <img src="img/articles/post-img-1.jpg" alt="">
      </div>

      <div class="u-form__logo__circle">
        <svg aria-hidden="true" width="8" height="6">
          <use xlink:href="img/svgs.svg#i-checked"></use>
        </svg>
      </div>
    </div>

    <div class="u-form-title">Статья добавлена<br> в корзину</div>
  </div>

  <div class="u-wd-500">

    <div class="u-add-to-cart__form">

      <ul class="u-add-to-cart__list">
        <li>
          <a href="#" title="" class="c-btn is-bg-black u-link-continue">Продолжить</a>
        </li>

        <li>
          <a href="cart.php" title="" class="c-btn is-bg-yellow u-link-go-to-basket">перейти в корзину</a>
        </li>
      </ul>

      <p>После оплаты Вы получите на почту письмо с доступом.<br>
        Lorem Ipsum - это текст-"рыба", часто используемый<br>
        в печати и вэб-дизайне.</p>
    </div>
  </div>
</div>