<div class="b-popup b-popup-request mfp-with-anim">

  <div class="u-form__head">

    <div class="u-form__logo">

      <div class="u-form__logo__figure">
        <img src="img/main/main-about-photo.jpg" alt="">
      </div>
    </div>

    <div class="u-form-title">Записаться</div>

    <p>Для записи на консультацию напишите мне<br> в одну из социальных сетей</p>

    <ul class="u-request__social__list">
      
      <li>
        
        <a href="#" title="" class="is-link-instagram">

          <div class="u-request__icon">
            <svg aria-hidden="true" width="26" height="26">
              <use xlink:href="img/svgs.svg#i-instagram"></use>
            </svg>
          </div>

          <div class="u-request__name__link">Instagram</div>
        </a>
      </li>

      <li>
        
        <a href="#" title="" class="is-link-vk">

          <div class="u-request__icon">
            <svg aria-hidden="true" width="23" height="15">
              <use xlink:href="img/svgs.svg#i-vk"></use>
            </svg>
          </div>

          <div class="u-request__name__link">Вконтакте</div>
        </a>
      </li>

      <li>
        
        <a href="#" title="" class="is-link-facebook">

          <div class="u-request__icon">
            <svg aria-hidden="true" width="11" height="23">
              <use xlink:href="img/svgs.svg#i-facebook"></use>
            </svg>
          </div>

          <div class="u-request__name__link">Facebook</div>
        </a>
      </li>

      <li>
        
        <a href="#" title="" class="is-link-ok">

          <div class="u-request__icon">
            <svg aria-hidden="true" width="15" height="23">
              <use xlink:href="img/svgs.svg#i-ok"></use>
            </svg>
          </div>

          <div class="u-request__name__link">Одноклассники</div>
        </a>
      </li>

      <li>
        
        <a href="#" title="" class="is-link-google">

          <div class="u-request__icon">
            <svg aria-hidden="true" width="20" height="20">
              <use xlink:href="img/svgs.svg#i-google"></use>
            </svg>
          </div>

          <div class="u-request__name__link">Google</div>
        </a>
      </li>
    </ul>
  </div>

  <div class="u-form__foot">

    <p>По всем техническим вопросам и предложениям пишите на почту:</p>
    <p><a href="mailto:info@veronikastepanova.com" title="">info@veronikastepanova.com</a></p>
  </div>
</div>