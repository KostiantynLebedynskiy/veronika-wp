<div class="b-popup b-popup-seo mfp-with-anim">

  <div class="u-form__head">
    <div class="u-form-title">Ваш заказ успешно оформлен!</div>
  </div>
	
	<div class="u-wd-500">
  	<p>Спасибо! Ваш заказ получен нами. Наш менеджер свяжется с Вами в ближайшее время. Если у Вас есть вопросы - свяжитесь, пожалуйста, с нами удобным для Вас способом.</p>
  </div>
</div>