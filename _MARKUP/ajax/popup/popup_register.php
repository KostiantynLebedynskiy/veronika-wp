<div class="b-popup b-popup-register mfp-with-anim">

  <div class="u-form__head">

    <div class="u-form__logo">
      
      <div class="u-form__logo__figure">
        
        <svg aria-hidden="true" width="19" height="20">
          <use xlink:href="img/svgs.svg#i-login"></use>
        </svg>
      </div>

      <div class="u-form__logo__circle">
        <svg aria-hidden="true" width="8" height="8">
          <use xlink:href="img/svgs.svg#i-plus"></use>
        </svg>
      </div>
    </div>

    <div class="u-form-title">Зарегистрироваться</div>
  </div>

  <div class="u-wd-500">

    <div class="u-register__form">

      <form class="u-form u-form-register" autocomplete="off">

        <div class="u-form__item">

          <label class="u-form__label" for="register_name">Имя</label>
          <input type="text" class="u-input-style" id="register_name" name="register_name">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">

          <label class="u-form__label" for="register_surname">Фамилия</label>
          <input type="text" class="u-input-style" id="register_surname" name="register_surname">
          <div class="u-text-error"></div>
        </div>
        
        <div class="u-form__item">

          <label class="u-form__label" for="register_email">Эл. почта</label>
          <input type="text" class="u-input-style" id="register_email" name="register_email">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">

          <label class="u-form__label" for="register_password">Пароль</label>
          <input type="password" class="u-input-style" id="register_password" name="register_password">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">

          <label class="u-form__label" for="register_password_repeat">Повторите пароль</label>
          <input type="password" class="u-input-style" id="register_password_repeat" name="register_password_repeat">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">
          
          <div class="u-form-agreement is-form-agreement">

            <label class="u-checkbox__agreement__label">
              <input type="checkbox" class="u-checkbox__agreement__input" name="register_agreement">
              <span class="u-checkbox__agreement__section">
                <span class="u-checkbox__agreement__box">
                  <svg aria-hidden="true" width="13" height="9">
                    <use xlink:href="img/svgs.svg#i-checked"></use>
                  </svg>
                </span>
                <span class="u-checkbox__agreement__name">Даю согласие на обработку своих <a href="#" title="">персональных данных</a></span>
              </span>
            </label>
          </div>
        </div>

        <div class="u-form__item">

          <button type="submit" class="c-btn is-bg-yellow u-btn-register">
            <span class="is-submit-title">Зарегистрироваться</span>

            <span class="is-loader">
              <span class="is-loader-circle"></span>
            </span>
          </button>
        </div>
      </form>
    </div>

    <div class="u-enter-site__forgot">

      <ul class="u-enter-site__forgot__list">

        <li>
          <a href="#" title="" class="u-link-enter-site">Войти</a>
        </li>
      </ul>
    </div>
  </div>
</div>