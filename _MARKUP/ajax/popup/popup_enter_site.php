<div class="b-popup b-popup-enter-site mfp-with-anim">

  <div class="u-form__head">

    <div class="u-form__logo">

      <div class="u-form__logo__figure">

        <svg aria-hidden="true" width="19" height="20">
          <use xlink:href="img/svgs.svg#i-login"></use>
        </svg>
      </div>

      <div class="u-form__logo__circle">
        <svg aria-hidden="true" width="8" height="6">
          <use xlink:href="img/svgs.svg#i-checked"></use>
        </svg>
      </div>
    </div>

    <div class="u-form-title">Войти</div>
  </div>

  <div class="u-wd-500">

    <div class="u-enter-site__form">

      <form class="u-form u-form-enter-site">
      
        <div class="u-form__item">

          <label class="u-form__label"login">Эл. почта</label>
          <input type="text" class="u-input-style" id="login_email" name="login_email">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">

          <label class="u-form__label" for="login_password">Пароль</label>
          <input type="password" class="u-input-style" id="login_password" name="login_password">
          <div class="u-text-error"></div>
        </div>

        <div class="u-form__item">

          <button type="submit" class="c-btn is-bg-yellow u-btn-enter-site">
            <span class="is-submit-title">Войти</span>

            <span class="is-loader">
              <span class="is-loader-circle"></span>
            </span>
          </button>
        </div>
      </form>
    </div>

    <div class="u-enter-site__forgot">

      <ul class="u-enter-site__forgot__list">
        <li>
          <a href="#" title="">Забыли пароль?</a>
        </li>

        <li>
          <a href="#" title="" class="u-link-register">Зарегистрироваться</a>
        </li>
      </ul>
    </div>

    <div class="u-enter-site__sn">

      <p>Быстрая авторизация с помощью<br> социальных сетей:</p>

      <ul class="u-enter-site__share__list">
        <li>
          <a href="#" title="" class="u-link-facebook">
            <svg aria-hidden="true" width="7" height="13">
              <use xlink:href="img/svgs.svg#i-facebook"></use>
            </svg>
          </a>
        </li>

        <li>
          <a href="#" title="" class="u-link-vk">
            <svg aria-hidden="true" width="14" height="9">
              <use xlink:href="img/svgs.svg#i-vk"></use>
            </svg>
          </a>
        </li>

        <li>
          <a href="#" title="" class="u-link-instagram">
            <svg aria-hidden="true" width="16" height="16">
              <use xlink:href="img/svgs.svg#i-instagram"></use>
            </svg>
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>