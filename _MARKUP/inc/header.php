﻿<!DOCTYPE html>
<html lang="ru">
  <head>
    <title>Психологическая онлайн-школа Вероники Степаново</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
    <meta name="description" content="Вероника Степанова">
    <meta name="theme-color" content="#fdb72a">
    <meta name="msapplication-navbutton-color" content="#fdb72a">
    <meta name="apple-mobile-web-app-status-bar-style" content="#fdb72a">
    <meta name="msapplication-TileColor" content="#fdb72a">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">

    <!-- FAVICONS -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">

    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="fonts/fonts.css">
    
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/lib/slick.css">
    <link rel="stylesheet" type="text/css" href="css/lib/select2.css">
    <link rel="stylesheet" type="text/css" href="css/lib/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css" href="css/lib/magnific-popup.min.css">
    <link rel="stylesheet" type="text/css" href="css/global.css">
  </head>

  <body
    <?php
      $url = explode('/', $_SERVER['REQUEST_URI']);
      if($url[2] =='account.php' || $url[2] =='account_history.php') {
    ?>
    class="u-page-account"
    <?php
      }
    ?>
    <?php
      $url = explode('/', $_SERVER['REQUEST_URI']);
      if($url[2] =='test_detail_result.php' || $url[2] =='test_detail_intro.php' || $url[2] =='test_detail_test.php' || $url[2] =='articles_detail.php' || $url[2] =='articles_detail_pay.php' || $url[2] =='search.php') {
    ?>
    class="u-page-inner"
    <?php
      }
    ?>
  >
    <!--[if lte IE 9]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
    <![endif]-->

    <div class="u-preloader"></div>

    <div id="wrapper" class="c-wrapper">

      <header id="header" class="c-header">

        <div class="container">
          
          <div class="c-header__row">
            
            <div class="c-header__item c-header__item__logo">

              <?php
                $url = explode('/', $_SERVER['REQUEST_URI']);
                if(empty($url[2]) || $url[2] =='index.php' || $url[2] =='main.php') {
              ?>
              <span class="c-header__logo">
                <img src="img/logo.svg" alt="">
              </span>
              <?php
                } else {
              ?>
              <a href="index.php" title="" class="c-header__logo">
                <img src="img/logo.svg" alt="">
              </a>
              <?php
                }
              ?>
            </div>

            <div class="c-header__item c-header__item__menu">

              <nav class="c-header__nav">
                <ul>
                  <li><a href="about.php" title="">Об авторе</a></li>
                  <li><a href="vebinar_featured.php" title="">Видеоуроки и вебинары</a></li>
                  <li><a href="articles.php" title="">Мои статьи</a></li>
                  <li><a href="gallery.php" title="">Видеогалерея</a></li>
                  <!-- <li><a href="#" title="">Ещё</a></li> -->
                </ul>
              </nav>

              <div class="c-header__nav__icon u-link-more">
                <span class="c-header__nav__icon__link">Ещё</span>

                <div class="c-header__nav__icon__figure">
                  <svg aria-hidden="true" width="25" height="12">
                    <path fill="currentColor" d="M0,12v-2h25v2H0z M0,0h25v2H0V0z"/>
                  </svg>
                </div>
              </div>

              <div class="c-header__menu__close">
                <svg aria-hidden="true" width="20" height="20">
                  <use xlink:href="img/svgs.svg#i-menu-close"></use>
                </svg>
              </div>
            </div>

            <div class="c-header__item c-header__item__option">

              <span class="c-btn is-yellow u-btn-sign-up is-desktop u-link-request">записаться</span>

              <ul class="c-header__option__list">

                <li>
                  <a href="search.php" title="" class="u-header__option__link u-link-search">
                    <svg aria-hidden="true" width="20" height="20">
                      <use xlink:href="img/svgs.svg#i-search"></use>
                    </svg>
                  </a>
                </li>

                <li>
                  <span class="u-header__option__link u-link-login">
                    <svg aria-hidden="true" width="19" height="20">
                      <use xlink:href="img/svgs.svg#i-login"></use>
                    </svg>
                  </span>
                  <span class="u-link-login__dropdown">
                    <ul>
                      <li><a href="account.php" title="">Мой аккаунт</a></li>
                      <li><a href="account_history.php" title="">История покупок</a></li>
                      <li><a href="index.php" title="">Выход</a></li>
                    </ul>
                  </span>
                </li>

                <li>
                  <a href="cart.php" title="" class="u-header__option__link u-link-cart">
                    <i>3</i>
                    <svg aria-hidden="true" width="18" height="22">
                      <use xlink:href="img/svgs.svg#i-cart"></use>
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <div class="c-header__menu">

          <div class="c-header__menu__row">
          
            <div class="container">
              
              <div class="c-header__menu__section">
                
                <div class="row">
                  
                  <div class="col-sm-12 col-lg-3 c-header__menu__item u-item-hidden">

                    <div class="c-header__menu__title">
                      <a href="index.php" title="">Главная</a>
                    </div>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                    <div class="c-header__menu__title">
                      <a href="about.php" title="">Об авторе</a>

                      <div class="expland">
                        <svg aria-hidden="true" width="10" height="6">
                          <path fill="currentColor" d="M4.5,5.8L0.2,1.4C0.1,1.2,0,1,0,0.8c0-0.2,0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l2.9,3V3.2C4.5,3.4,4.8,3.7,5,3.7c0.2,0,0.5-0.2,0.8-0.5v0.1l2.9-3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L5.6,5.8C5.3,6.1,4.8,6.1,4.5,5.8z"/>
                        </svg>
                      </div>
                    </div>

                    <ul>
                      <li><a href="contacts.php" title="">Контакты</a></li>
                    </ul>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                    <div class="c-header__menu__title">
                      <a href="vebinar_featured.php" title="">Видеоуроки и вебинары</a>

                      <div class="expland">
                        <svg aria-hidden="true" width="10" height="6">
                          <path fill="currentColor" d="M4.5,5.8L0.2,1.4C0.1,1.2,0,1,0,0.8c0-0.2,0.1-0.4,0.2-0.6c0.3-0.3,0.8-0.3,1.1,0l2.9,3V3.2C4.5,3.4,4.8,3.7,5,3.7c0.2,0,0.5-0.2,0.8-0.5v0.1l2.9-3c0.3-0.3,0.8-0.3,1.1,0c0.3,0.3,0.3,0.8,0,1.1L5.6,5.8C5.3,6.1,4.8,6.1,4.5,5.8z"/>
                        </svg>
                      </div>
                    </div>

                    <ul>
                      <li><a href="vebinar_past.php" title="">Вебинары</a></li>
                      <li><a href="video_lessons_trainings.php" title="">Видеоуроки и тренинги</a></li>
                      <li><a href="training_psychologists.php" title="">Обучение для психологов</a></li>
                    </ul>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                     <div class="c-header__menu__title">
                      <a href="articles.php" title="">Мои статьи</a>
                    </div>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                     <div class="c-header__menu__title">
                      <a href="#" title="">Электронные книги</a>
                    </div>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                     <div class="c-header__menu__title">
                      <a href="test.php" title="">Тесты</a>
                    </div>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item">
                    
                     <div class="c-header__menu__title">
                      <a href="gallery.php" title="">Видеогалерея</a>
                    </div>
                  </div>

                  <div class="col-sm-12 col-lg-3 c-header__menu__item u-item-hidden">
                    
                    <div class="c-header__menu__title">
                      <a href="#" title="">Сувениры</a>
                    </div>
                  </div>
                  
                  <div class="col-sm-12 col-lg-3 c-header__menu__item">

                    <span class="c-btn is-yellow u-btn-sign-up is-mobile u-link-request">записаться</span>
                  </div>
                </div>
                
                <div class="c-header__menu__social">
                  
                  <ul>
                    
                    <li>
                      
                      <a href="#" title="" class="is-link-youtube">
                        <svg aria-hidden="true" width="14" height="11">
                          <use xlink:href="img/svgs.svg#i-youtube"></use>
                        </svg>
                        <span>YouTube</span>
                      </a>
                    </li>

                    <li>
                      
                      <a href="#" title="" class="is-link-instagram">
                        <svg aria-hidden="true" width="16" height="16">
                          <use xlink:href="img/svgs.svg#i-instagram"></use>
                        </svg>
                        <span>Instagram</span>
                      </a>
                    </li>

                    <li>
                      
                      <a href="#" title="" class="is-link-vk">
                        <svg aria-hidden="true" width="14" height="9">
                          <use xlink:href="img/svgs.svg#i-vk"></use>
                        </svg>
                        <span>Вконтакте</span>
                      </a>
                    </li>

                    <li>
                      
                      <a href="#" title="" class="is-link-facebook">
                        <svg aria-hidden="true" width="6" height="12">
                          <use xlink:href="img/svgs.svg#i-facebook"></use>
                        </svg>
                        <span>Facebook</span>
                      </a>
                    </li>

                    <li>
                      
                      <a href="#" title="" class="is-link-ok">
                        <svg aria-hidden="true" width="11" height="17">
                          <use xlink:href="img/svgs.svg#i-ok"></use>
                        </svg>
                        <span>Одноклассники</span>
                      </a>
                    </li>

                    <li>
                      
                      <a href="#" title="" class="is-link-google">
                        <svg aria-hidden="true" width="12" height="12">
                          <use xlink:href="img/svgs.svg#i-google"></use>
                        </svg>
                        <span>Google</span>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </header><!--/#header -->

      <main id="content" class="c-content">