			</main><!--/#content -->

			<?php
        $url = explode('/', $_SERVER['REQUEST_URI']);
        if($url[2] != '404.php') {
      ?>
			<div class="c-footer-wrapper">
		    <footer id="footer" class="c-footer">

		    	<div class="c-footer__top">

			      <div class="container">

			        <nav class="c-footer__top__nav">
			        	<ul>
			        		<li><a href="about.php" title="">Об авторе</a></li>
			        		<li><a href="#" title="">Видеоуроки и вебинары</a></li>
			        		<li><a href="articles.php" title="">Мои статьи</a></li>
			        		<li><a href="#" title="">Электронные книги</a></li>
			        		<li><a href="test.php" title="">Тесты</a></li>
			        		<li><a href="#" title="">Видеогалерея</a></li>
			        		<li><a href="#" title="">Сувениры</a></li>
			        	</ul>
			        </nav>
			      </div>
			    </div>

			    <a href="#" title="" class="u-link-sign-up__mobile u-link-request">записаться</a>

			    <div class="c-footer__bottom">
		    		
			      <div class="container">

			      	<div class="c-footer__bottom__form">

			      		<div class="u-width-500">
			      			
			      			<form class="u-form u-form-keep-update" autocomplete="off">
			      				
			      				<div class="u-form__title">Будь в курсе новостей</div>

			      				<div class="u-form-item">

			      					<input type="text" class="u-input-style" id="keep_update" name="keep_update" inputmode="email" placeholder="Ваш e-mail" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Ваш e-mail'">

			      					<div class="u-text-error"></div>

			      					<button type="submit" class="c-btn u-btn-submit u-btn-keep-update">
			      						<span class="is-submit-title">подписаться</span>

			      						<span class="is-loader">
			      							<span class="is-loader-circle"></span>
			      						</span>
			      					</button>
			      				</div>

			      				<p>Я не рассылаю спам</p>
			      			</form>
			      		</div>
			      	</div>

			        <div class="c-footer__bottom__dev">
			        	
			        	<div class="row">
			        		
			        		<div class="col-sm-12 col-md-3 u-footer__item u-footer__item__dev">
			        			
			        			<p>
			        				<a href="https://ru.flamix.software/" target="_blank" title="FLAMIX" class="c-footer__dev">
		              			Разработка сайта
		              		
			            			<svg aria-hidden="true" width="55" height="11">
													<path class="fx-color-white" d="M54.5,10.6c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.6-0.1-0.9-0.4l-3.3-3.3l-3.3,3.3c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.6-0.1-0.9-0.4c-0.5-0.5-0.5-1.3,0-1.8l3.3-3.3l-3.3-3.3c-0.5-0.5-0.5-1.3,0-1.8c0.5-0.5,1.3-0.5,1.8,0l8.3,8.4C55,9.3,55,10.1,54.5,10.6zM41.6,11c-0.7,0-1.2-0.6-1.2-1.3V1.3c0-0.7,0.6-1.3,1.2-1.3c0.7,0,1.2,0.6,1.2,1.3v8.4C42.8,10.4,42.2,11,41.6,11z M37,11c-0.7,0-1.2-0.6-1.2-1.3V4.4l-2,2.1c-0.5,0.5-1.3,0.5-1.8,0l-2-2.1v5.4c0,0.7-0.6,1.3-1.2,1.3c-0.7,0-1.2-0.6-1.2-1.3V1.3c0-0.5,0.3-1,0.8-1.2c0.5-0.2,1-0.1,1.4,0.3l3.3,3.3l3.3-3.3C36.5,0,37-0.1,37.5,0.1c0.5,0.2,0.8,0.7,0.8,1.2v8.4C38.2,10.4,37.7,11,37,11z M25.4,11c-0.5,0-0.9-0.3-1.1-0.7l-3-6.2l-3,6.2C17.9,10.8,17.5,11,17,11h-5.8c-0.7,0-1.2-0.6-1.2-1.3V1.3C10,0.6,10.5,0,11.2,0c0.7,0,1.2,0.6,1.2,1.3v7.2h3.8l3.8-7.7C20.3,0.3,20.7,0,21.2,0c0.5,0,0.9,0.3,1.1,0.7l4.2,8.4c0.3,0.6,0.1,1.4-0.6,1.7C25.7,11,25.5,11,25.4,11z M7.9,2.6H2.5v3h3.4c0.7,0,1.2,0.6,1.2,1.3C7.2,7.5,6.6,8,5.9,8H2.5v1.7c0,0.7-0.6,1.3-1.2,1.3S0,10.4,0,9.7V1.3C0,0.6,0.6,0,1.2,0h6.6c0.7,0,1.2,0.6,1.2,1.3C9.1,2,8.6,2.6,7.9,2.6z"/>
													<path class="fx-color-orange" d="M54.6,2.2L53,3.9c-0.2,0.2-0.6,0.4-0.9,0.4s-0.6-0.1-0.9-0.4c-0.5-0.5-0.5-1.3,0-1.8l1.7-1.7c0.5-0.5,1.3-0.5,1.8,0C55.1,0.9,55.1,1.7,54.6,2.2zM21.3,10.1c-0.7,0-1.2-0.6-1.2-1.3c0-0.7,0.6-1.3,1.2-1.3c0.7,0,1.2,0.6,1.2,1.3C22.6,9.6,22,10.1,21.3,10.1z"/>
			                  </svg>
			            		</a>
			        			</p>
			        		</div>

			        		<div class="col-sm-12 col-md-12 col-lg-6 u-footer__item">
			        			
			        			<p>Копирование любых материалов с сайта без согласия автора запрещено.</p>
			        			<p>© 2019 Вероника Степанова</p>

										<a href="https://ru.flamix.software/" target="_blank" title="FLAMIX" class="c-footer__dev is-mobile">		              		
		            			<svg aria-hidden="true" width="55" height="11">
												<path class="fx-color-white" d="M54.5,10.6c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.6-0.1-0.9-0.4l-3.3-3.3l-3.3,3.3c-0.2,0.2-0.6,0.4-0.9,0.4c-0.3,0-0.6-0.1-0.9-0.4c-0.5-0.5-0.5-1.3,0-1.8l3.3-3.3l-3.3-3.3c-0.5-0.5-0.5-1.3,0-1.8c0.5-0.5,1.3-0.5,1.8,0l8.3,8.4C55,9.3,55,10.1,54.5,10.6zM41.6,11c-0.7,0-1.2-0.6-1.2-1.3V1.3c0-0.7,0.6-1.3,1.2-1.3c0.7,0,1.2,0.6,1.2,1.3v8.4C42.8,10.4,42.2,11,41.6,11z M37,11c-0.7,0-1.2-0.6-1.2-1.3V4.4l-2,2.1c-0.5,0.5-1.3,0.5-1.8,0l-2-2.1v5.4c0,0.7-0.6,1.3-1.2,1.3c-0.7,0-1.2-0.6-1.2-1.3V1.3c0-0.5,0.3-1,0.8-1.2c0.5-0.2,1-0.1,1.4,0.3l3.3,3.3l3.3-3.3C36.5,0,37-0.1,37.5,0.1c0.5,0.2,0.8,0.7,0.8,1.2v8.4C38.2,10.4,37.7,11,37,11z M25.4,11c-0.5,0-0.9-0.3-1.1-0.7l-3-6.2l-3,6.2C17.9,10.8,17.5,11,17,11h-5.8c-0.7,0-1.2-0.6-1.2-1.3V1.3C10,0.6,10.5,0,11.2,0c0.7,0,1.2,0.6,1.2,1.3v7.2h3.8l3.8-7.7C20.3,0.3,20.7,0,21.2,0c0.5,0,0.9,0.3,1.1,0.7l4.2,8.4c0.3,0.6,0.1,1.4-0.6,1.7C25.7,11,25.5,11,25.4,11z M7.9,2.6H2.5v3h3.4c0.7,0,1.2,0.6,1.2,1.3C7.2,7.5,6.6,8,5.9,8H2.5v1.7c0,0.7-0.6,1.3-1.2,1.3S0,10.4,0,9.7V1.3C0,0.6,0.6,0,1.2,0h6.6c0.7,0,1.2,0.6,1.2,1.3C9.1,2,8.6,2.6,7.9,2.6z"/>
												<path class="fx-color-orange" d="M54.6,2.2L53,3.9c-0.2,0.2-0.6,0.4-0.9,0.4s-0.6-0.1-0.9-0.4c-0.5-0.5-0.5-1.3,0-1.8l1.7-1.7c0.5-0.5,1.3-0.5,1.8,0C55.1,0.9,55.1,1.7,54.6,2.2zM21.3,10.1c-0.7,0-1.2-0.6-1.2-1.3c0-0.7,0.6-1.3,1.2-1.3c0.7,0,1.2,0.6,1.2,1.3C22.6,9.6,22,10.1,21.3,10.1z"/>
		                  </svg>
		            		</a>
			        		</div>

			        		<div class="col-sm-12 col-md-3 u-footer__item u-footer__item__signup">
			        			
			        			<p><a href="#" title="" class="u-link-sign-up u-link-request">записаться</a></p>
			        		</div>
			        	</div>
			        </div>
			      </div>
			    </div>
		    </footer><!--/#footer -->
		  </div>
			<?php
        }
      ?>

		  <div class="u-overlay"></div>

		</div><!--/#wrapper -->

		<div class="u-tooltip-popup__fixed">
		  <div class="u-product__tags__more__list"></div>
		</div>

    <a href="#b-top" class="u-btn-scroll-top" title="">
      <svg width="12" height="8">
        <path d="M10.5,7.7L6,3.2L1.6,7.7L0,6l4.4-4.4l0,0L6,0l6,6L10.5,7.7z" fill="currentColor"/>
      </svg>
    </a>

		<script src="//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/lib/jquery.min.js"><\/script>')</script>
		<script src="js/lib/svg4everybody.min.js"></script>
    <script src="js/lib/jquery.placeholder.min.js"></script>
    <script src="js/lib/smooth-scroll.min.js"></script>
    <script src="js/lib/validate.min.js"></script>
    <script src="js/lib/slick.min.js"></script>
    <script src="js/lib/jquery.mousewheel.min.js"></script>
    <script src="js/lib/select2.full.min.js"></script>
    <script src="js/lib/jquery.waypoints.min.js"></script>
    <script src="js/lib/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="js/lib/jquery.matchHeight-min.js"></script>
    <script src="js/lib/jquery.magnific-popup.min.js"></script>
    <script src="js/lib/isotope.pkgd.min.js"></script>
    <script src="js/lib/jquery.inputmask.bundle.js"></script>

    <script src="js/common/slider.js"></script>
    <script src="js/common/validate.js"></script>
    <script src="js/common/select.js"></script>
    <script src="js/common/lazy_load.js"></script>
    <script src="js/common/popup.js"></script>
    <script src="js/common/mask.js"></script>
    <script src="js/common/scroll-top.js"></script>
		
		<script src="js/dev.js"></script>
	</body>

	<?php

    /**
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_enter_site
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_register
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_request
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_add_to_cart
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_glossary_word
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_subscribed
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_message_sent
    *   http://markup.develop.flamix.info/veronikastepanova/index.php?action=popup_order_success
    */

    if ($_GET) {
      switch ($_GET['action']) {

        //Войти
        case 'popup_enter_site':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_enter_site.php'
              });
            </script>
          <?php
        break;

        //Зарегистрироваться
        case 'popup_register':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_register.php'
              });
            </script>
          <?php
        break;

        //Записаться
        case 'popup_request':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_request.php'
              });
            </script>
          <?php
        break;

        //Добавлено в корзину
        case 'popup_add_to_cart':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_add_to_cart.php'
              });
            </script>
          <?php
        break;

        //Термины
        case 'popup_glossary_word':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_glossary_word.php'
              });
            </script>
          <?php
        break;

        //Вы подписались на рассылку
        case 'popup_subscribed':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_subscribed.php'
              });
            </script>
          <?php
        break;

        //Благодарим за обращение
        case 'popup_message_sent':
          ?>
            <script>
              popup({
              	el: 'ajax/popup/popup_message_sent.php'
              });
            </script>
          <?php
        break;

        //Ваш заказ успешно оформлен
        case 'popup_order_success':
          ?>
            <script>
              popup({
                el: 'ajax/popup/popup_order_success.php'
              });
            </script>
          <?php
        break;

        default:
          break;
      }
    }
   ?>
</html>