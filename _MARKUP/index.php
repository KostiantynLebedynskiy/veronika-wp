<?php 
	//Хеадер
  include 'inc/header.php';

  //Контент
  include 'component/main/main_slider.php';
  include 'component/main/main_about.php';
  include 'component/main/events.php';
  include 'component/main/video_lessons.php';
  include 'component/main/psychologists.php';
  include 'component/other/articles.php';
  include 'component/main/book.php';
  include 'component/main/catalog.php';
  include 'component/main/video.php';

  include 'component/follow/index.php';
  
  //Футер
  include 'inc/footer.php';
?>