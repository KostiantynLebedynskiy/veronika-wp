// DEV.JS
//------------------------------------------------------------------------------------------------
// This is DEV JS file that contains custom JS scipts and initialization used in this template */
// -----------------------------------------------------------------------------------------------
// Template Name: DEV
// Author: Vasyl Makohin
// Version 1.0 - Initial Release
// Copyright: (C) 2019
// -----------------------------------------------------------------------------------------------
'use strict';

const Veronikastepanova = function () {

  this.$document = $(document);
  this.$window   = $(window);
}

Veronikastepanova.prototype.init = function () {

  var self = this;

  //svg4everybody
    svg4everybody();

  //placeholder
    $('input, textarea').placeholder();

  //preventDefault
    self.$document.on('click', 'a[href="#"]', function(e){
      e.preventDefault();
    });

    self.headerSticky();
    self.headerMenu();
    self.slider();
    self.tooltipCustom();
    self.selectStyle();
    self.scrollStyle();
    self.accountChangePhoto();
    self.accountHistoryToogle();
    self.gallerySort();
    self.popups();
    self.loginDropdown();
    self.reviewsGrid();
    maskInputs();
    self.tabs({
      tabsHead: '.c-cart__tabs__navigator',
      tabsBody: '.c-cart__tabs__content'
    });
};

Veronikastepanova.prototype.headerSticky = function() {

  var self = this,
      $header = $('.c-header'),
      scrollTop = 0;

      headerInit();

      $(window).on('scroll', function () {
        headerInit();
      });

      function headerInit() {
        scrollTop = $(window).scrollTop();
        if (scrollTop > 0) {
          $header.addClass('is-sticky');
        } else {
          $header.removeClass('is-sticky');
        }
      }
};

Veronikastepanova.prototype.headerMenu = function() {

  var self = this,
      $btnMore = $('.u-link-more'),
      $btnClose = $('.c-header__menu__close'),
      $section = $('.c-wrapper'),
      $window = $(window),
      $body = $('body'),
      $offsetY = window.pageYOffset,
      $expland = $('.expland'),
      $mobileNav = $('.c-header__menu__section');

      $btnMore.on('click', function(e) {
        e.preventDefault();

        $btnMore.addClass('is-hidden');
        $section.addClass('is-open');

        /**
          * Запретить скроллинг
          */

          if ($window.width() < 1024) {
              $offsetY = window.pageYOffset;
              $body.css({
                'position': 'fixed',
                'top': -$offsetY + 'px'
              });
          }else{
              $body.css({
                'position': 'static',
                'top': ''
              });
          }
      });

      $btnClose.on('click', function(e) {
        e.preventDefault();

        $btnMore.removeClass('is-hidden');
        $section.removeClass('is-open');

         /**
          * Разблокировать скроллинг
          */
          if ($window.width() < 1024) {
              $body.css({
                'position': 'static',
                'top': ''
              });
              $window.scrollTop($offsetY);
          }else{
              $body.css({
                'position': 'static',
                'top': ''
              });
          }

          /**
            * Закрыть все вкладки меню
            */
            $mobileNav.find('.c-header__menu__item').removeClass('is-open');

            if ($window.width() > 1024) {
                $mobileNav.find('.c-header__menu__item').children('ul').fadeIn();
            }else{
                $mobileNav.find('.c-header__menu__item').children('ul').slideUp();
            }
            $mobileNav.find('.expland').removeClass('is-active');
      });

      $expland.on('click', function(e) {
        e.preventDefault();

        var $this = $(this);
        
        if ($this.hasClass('is-active')) {

            $this.removeClass('is-active');
            $this.parents('.c-header__menu__item').removeClass('is-open');
            $this.parents('.c-header__menu__item').children('ul').slideUp();
        }
        else{

            $mobileNav.find('.c-header__menu__item').removeClass('is-open');
            $mobileNav.find('.c-header__menu__item').children('ul').slideUp();
            $mobileNav.find('.expland').removeClass('is-active');

            $this.addClass('is-active');
            $this.parents('.c-header__menu__item').addClass('is-open');
            $this.parents('.c-header__menu__item').children('ul').slideDown();
        }
      });

      $window.on('resize', function() {

        if ($window.width() > 1024) {
            
            /**
              * Закрыть меню
              */
              $btnMore.removeClass('is-hidden');
              $section.removeClass('is-open');

            /**
              * Разблокировать скроллинг
              */
              $body.css({
                'position': 'static',
                'top': ''
              });

            /**
              * Закрыть все вкладки меню
              */
              $mobileNav.find('.c-header__menu__item').removeClass('is-open');
              $mobileNav.find('.c-header__menu__item').children('ul').fadeIn();
              $mobileNav.find('.expland').removeClass('is-active');
        }else{

              $mobileNav.find('.c-header__menu__item').removeClass('is-open');
              $mobileNav.find('.c-header__menu__item').children('ul').fadeOut();
              $mobileNav.find('.expland').removeClass('is-active');
        }
      });

      $(document).on('click', '.c-wrapper.is-open .u-overlay', function(e) {
        e.preventDefault();

        $btnMore.removeClass('is-hidden');
        $section.removeClass('is-open');

         /**
          * Разблокировать скроллинг
          */
          if ($window.width() < 1024) {
              $body.css({
                'position': 'static',
                'top': ''
              });
              $window.scrollTop($offsetY);
          }else{
              $body.css({
                'position': 'static',
                'top': ''
              });
          }

          /**
            * Закрыть все вкладки меню
            */
            $mobileNav.find('.c-header__menu__item').removeClass('is-open');

            if ($window.width() > 1024) {
                $mobileNav.find('.c-header__menu__item').children('ul').fadeIn();
            }else{
                $mobileNav.find('.c-header__menu__item').children('ul').slideUp();
            }
            $mobileNav.find('.expland').removeClass('is-active');
      });
};

Veronikastepanova.prototype.slider = function () {

  if (!$.fn.slick) {
      return;
  }

  carouselSlider({
    el: '.u-section__events .u-style-slider'
  });

  carouselSlider({
    el: '.u-section__video__lessons .u-style-slider'
  });

  carouselSlider({
    el: '.u-section__psychologists .u-style-slider'
  });

  carouselSlider({
    el: '.u-section__articles .u-style-slider'
  });

  carouselSlider({
    el: '.u-section__catalog .u-style-slider'
  });

  videoSlider({
    el: '.u-section__video .u-style-slider'
  });

  articlesSlider({
    el: '.c-articles-detail__gallery .u-style-slider'
  });
};

Veronikastepanova.prototype.tooltipCustom = function () {

  var self = this;

    $('.u-product__tags__more').on('mouseenter touchstart', function(event) {

      $(this).find('.u-product__tags__more__link').addClass('is-active');

      var left = $(this).offset().left - $(window).scrollLeft();
      var top = $(this).offset().top - $(window).scrollTop();
      
      var $widthContainer = $(this).parents('.u-section__product__footer').innerWidth(),
          $cloneContainer = $(this).find('.u-list-hidden ul').clone();

          $('.u-tooltip-popup__fixed').css({
            'width': $widthContainer,
            'top': top,
            'left': left
          });

          $('.u-tooltip-popup__fixed').find('.u-product__tags__more__list').html($cloneContainer);

          $('.u-tooltip-popup__fixed').show();
    });

    $('.u-tooltip-popup__fixed').on('mouseleave', function(event) {
      $('.u-product__tags__more__link').removeClass('is-active');
      $(this).hide();
    });

    $(document).on('touchmove mousewheel', function() {
      $('.u-product__tags__more__link').removeClass('is-active');
      $('.u-tooltip-popup__fixed').hide();
    });
};

Veronikastepanova.prototype.selectStyle = function () {

  if (!$.fn.select2) {
        return;
  }

    selectInit({
      element: '.u-select-init',
      dropdownClass: 'u-html-select-init',
      search: -1
    });
};

Veronikastepanova.prototype.scrollStyle = function() {

  if (!$.fn.mCustomScrollbar) {
        return;
  }

      $('.u-scroll-tags-style').each(function() {
        
        $(this).mCustomScrollbar();
      });
};

Veronikastepanova.prototype.accountChangePhoto = function() {

  var self = this;

      $('.upload-photo').on('click', function(e){
        e.preventDefault();

        $('.imgInp').val("");

        $(this).parents('.c-account__user').find('.imgInp').click();
        $(this).parents('.c-account__user').find('.imgInp').change(function(){
          readURL(this);
        });
      });

      function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {

               $(input).parents('.c-account__user').find('.c-account__user__photo img').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
};

Veronikastepanova.prototype.accountHistoryToogle = function() {

  var self = this,
      btn = $('.u-history-order__head');

      btn.on('click', function() {
        
        var $this = $(this);

            if ($this.parents('.c-account-history__item').hasClass('is-open')) {
                $this.parents('.c-account-history__item').removeClass('is-open');
                $this.parents('.c-account-history__item').find('.u-history-order__body').slideUp();

            }else{

                $('.c-account-history__item').removeClass('is-open');
                $('.u-history-order__body').slideUp();

                $this.parents('.c-account-history__item').addClass('is-open');
                $this.parents('.c-account-history__item').find('.u-history-order__body').slideDown();
            }
      });
};

Veronikastepanova.prototype.gallerySort = function() {

  var self = this,
      $btn = $('.u-gallery__sort__link'),
      $section = $('.u-gallery__sort');

      $btn.on('click', function() {
        var $this = $(this);

        $section.removeClass('is-open');
        $btn.removeClass('is-active');
        
        if ($this.hasClass('is-active')) {
            $this.removeClass('is-active');
            $this.parents('.u-gallery__sort').removeClass('is-open');
        }else{
            $this.addClass('is-active');
            $this.parents('.u-gallery__sort').addClass('is-open');
        }
      });

      $('.u-btn-filter-apply-sort').on('click', function() {
        var $this = $(this),
            $value = $('.u-checkbox__input__sort:checked').val();

        $this.parents('.u-gallery__sort').find('.u-gallery__sort__link').removeClass('is-active');
        $('.u-gallery__sort__link__value').text($value);
        $section.removeClass('is-open');
      });

      $('.u-btn-filter-apply-rubrik').on('click', function() {
        var $this = $(this);

        $this.parents('.u-gallery__sort').find('.u-gallery__sort__link').removeClass('is-active').hide();        
        $('.u-gallery__sort__remove').addClass('is-sort');
        $section.removeClass('is-open');
      });

      $('.u-gallery__sort__remove__link').on('click', function() {
        $('.u-gallery__sort__remove').removeClass('is-sort');
        $btn.show();
      });

      $(document).on('click', function(event) {

        if (!$(event.target).closest('.u-gallery__sort__overlay').length) {
            $btn.removeClass('is-active');
            $section.removeClass('is-open');
        }
      });
};

Veronikastepanova.prototype.popups = function() {

  var self = this;

    if (!$.fn.magnificPopup) {
        return;
    }

    //Вход
      $(document).on('click', '.u-link-enter-site', function(e) {
        e.preventDefault();
          
          popup({
            el: 'ajax/popup/popup_enter_site.php'
          });
      });

    //Зарегистрироваться
      $(document).on('click', '.u-link-register', function(e) {
        e.preventDefault();
          
          popup({
            el: 'ajax/popup/popup_register.php'
          });
      });

    //Записаться
      $(document).on('click', '.u-link-request', function(e) {
        e.preventDefault();
          
          popup({
            el: 'ajax/popup/popup_request.php'
          });
      });

    //Добавлено в корзину
      $(document).on('click', '.u-link-add-to-cart', function(e) {
        e.preventDefault();
          
          popup({
            el: 'ajax/popup/popup_add_to_cart.php'
          });
      });

      $(document).on('click', '.u-link-continue', function(e) {
        e.preventDefault();
          
          $.magnificPopup.close();
      });

    //Термины
      $(document).on('click', '.c-glossary__all__list a', function(e) {
        e.preventDefault();
          
          popup({
            el: 'ajax/popup/popup_glossary_word.php'
          });
      });

      // glossaryPopup({
      //   el: $('.c-glossary__all__list ul'), 
      //   delegate: '.c-glossary__all__list ul li a',
      //   gallery: true
      // });
};

Veronikastepanova.prototype.loginDropdown = function() {

  var self = this,
      $btn = $('.u-link-login'),
      $dropdown = $('.u-link-login__dropdown');

      $btn.on('click', function() {

        var $this = $(this);
        
            if ($this.hasClass('is-active')) {
                $this.removeClass('is-active');
                $dropdown.removeClass('is-open');
            }else{
                $this.addClass('is-active');
                $dropdown.addClass('is-open');
            }
      });

      $(document).on('click', function(event) {

        if (!$(event.target).closest('.c-header__option__list').length) {
            $btn.removeClass('is-active');
            $dropdown.removeClass('is-open');
        }
      });

      $(document).on('touchmove mousewheel', function() {

        $btn.removeClass('is-active');
        $dropdown.removeClass('is-open');
      });
};

Veronikastepanova.prototype.reviewsGrid = function() {

  var self = this;

    if (!$.fn.isotope) {
        return;
    }

  var $container = $('.u-reviews__row'),
      item = '.u-reviews__item';

      $container.isotope({
        itemSelector : item
      });
};

Veronikastepanova.prototype.tabs = function (argument) {

  var self = this,
      $navTop = $(argument.tabsHead),
      $contentTop = $(argument.tabsBody);

      $navTop.find('[data-tab-link]').on('click', function () {

        var $this = $(this),
            $thisAttr = $(this).attr('data-tab-link');

        if (!$this.hasClass('is-active')) {

            $navTop.find('[data-tab-link]').removeClass('is-active');
            $contentTop.find('[data-tab-content]').removeClass('is-active');

            $this.addClass('is-active');
            $(argument.tabsBody + ' [data-tab-content='+ $thisAttr +']').addClass('is-active');
        }
      });
};

var veronikastepanova = new Veronikastepanova();

$(document).ready(function () {

  veronikastepanova.init();
  $('.u-preloader').fadeOut('slow');
});

$(window).on('load', function () {

  lazyLoadItems();
});