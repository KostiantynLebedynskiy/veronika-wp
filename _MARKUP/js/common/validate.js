$.fn.serializeObject = function() {
    "use strict";
    var a = {},
        b = function(b, c) {
            var d = a[c.name];
            "undefined" != typeof d && d !== null ? $.isArray(d) ? d.push(c.value) : a[c.name] = [d, c.value] : a[c.name] = c.value
        };
    return $.each(this.serializeArray(), b), a
};

$.fn.serializeFormJSON = function () {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function () {
    if (o[this.name]) {
      if (!o[this.name].push) {
        o[this.name] = [o[this.name]];
      }
      o[this.name].push(this.value || '');
    } else {
      o[this.name] = this.value || '';
    }
  });
  return o;
};

formValidation();

function formValidation() {

  var listEmail = ['test@mail.ru'];

  /*
  * Будь в курсе новостей
  **/
    var keepUpdateConfig = {
      keep_update: {
        presence: {
          message: "Неправильный e-mail"
        },
        email: {
          message: "Введите корректный e-mail"
        }
      }
    };

    $('.u-form-keep-update').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,keepUpdateConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"]').parents('.u-form-item').find('.u-text-error').addClass('u-not-valid').text(el);
          });

          if (jQuery.inArray($('[name="keep_update"]').val(), listEmail) !== -1) {
              $('[name="keep_update"]').addClass('u-not-valid');
              $('[name="keep_update"]').parents('.u-form-item').find('.u-text-error').addClass('u-not-valid').text('Такой e-mail уже существует');
              return false;
          }
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').val('').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);

              //Вы подписались на рассылку
              popup({
                el: 'ajax/popup/popup_subscribed.php'
              });
            }, 2000);
          }
    });

  /*
  * Напишите мне
  **/
    var contactsConfig = {
      contacts_name: {
        presence: {
          message: "Введите имя"
        }
      },
      contacts_email: {
        presence: {
          message: "Введите e-mail"
        },
        email: {
          message: "Введите корректный e-mail"
        }
      },
      contacts_messags: {
        presence: {
          message: "Введите сообщение"
        }
      }
    };

    $('.u-form-contacts').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,contactsConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"], textarea[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"], textarea[name="' + index + '"]').parents('.u-form-item').find('.u-text-error').addClass('u-not-valid').text(el);
          });

          if (jQuery.inArray($('[name="contacts_email"]').val(), listEmail) !== -1) {
              $('[name="contacts_email"]').addClass('u-not-valid');
              $('[name="contacts_email"]').parents('.u-form-item').find('.u-text-error').addClass('u-not-valid').text('Такой e-mail уже существует');
              return false;
          }
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').val('').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);


              //Благодарим за обращение
              popup({
                el: 'ajax/popup/popup_message_sent.php'
              });
            }, 2000);
          }
    });

  /*
  * Поиск
  **/
    var searchConfig = {
      search: {
        presence: {
          message: "Введите что искать"
        }
      }
    };

    $('.u-form-search').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,searchConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"], textarea[name="' + index + '"]').addClass('u-not-valid');
          });

        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').val('').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);
            }, 2000);
          }
    });

  /*
  * Мой аккаунт
  **/
    var accountConfig = {
      account_name: {
        presence: {
          message: "Введите имя"
        }
      },
      account_surname: {
        presence: {
          message: "Введите фамилию"
        }
      },
      account_email: {
        presence: {
          message: "Введите e-mail"
        },
        email: {
          message: "Введите корректный e-mail"
        }
      },
      account_city: {
        presence: {
          message: "Введите город"
        }
      },
      account_current_password: {
        presence: {
          message: "Введите текущий пароль"
        }
      },
      account_new_password: {
        presence: {
          message: "Введите новый пароль"
        }
      },
      account_repeat_password: {
        presence: {
          message: "Пароли не совпадают"
        },
        equality: {
          attribute: "account_new_password",
          message: "Пароли не совпадают"
        }
      }
    };

    $('.u-form-account').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,accountConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form__item').find('.u-text-error').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"]').parents('.u-form__item').find('.u-text-error').addClass('u-not-valid').text(el);
          });

          if (jQuery.inArray($('[name="account_email"]').val(), listEmail) !== -1) {
              $('[name="account_email"]').addClass('u-not-valid');
              $('[name="account_email"]').parents('.u-form__item').find('.u-text-error').addClass('u-not-valid').text('Такой e-mail уже существует');
              return false;
          }
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);
            }, 2000);
          }
    });

  /*
  * Войти
  **/
    var enterSiteConfig = {
      login_email: {
        presence: {
          message: "Введите e-mail"
        },
        email: {
          message: "Введите корректный e-mail"
        }
      },
      login_password: {
        presence: {
          message: "Введите пароль"
        }
      }
    };

    $('.u-form-enter-site').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,enterSiteConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form__item').find('.u-text-error').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"]').parents('.u-form__item').find('.u-text-error').addClass('u-not-valid').text(el);
          });
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);

              $.magnificPopup.close();
            }, 2000);
          }
    });

  /*
  * Зарегистрироваться
  **/
    var registerConfig = {
      register_name: {
        presence: {
          message: "Введите Имя"
        }
      },
      register_surname: {
        presence: {
          message: "Введите Фамилию"
        }
      },
      register_email: {
        presence: {
          message: "Введите e-mail"
        },
        email: {
          message: "Введите корректный e-mail"
        }
      },
      register_password: {
        presence: {
          message: "Введите пароль"
        }
      },
      register_password_repeat: {
        presence: {
          message: "Пароли не совпадают"
        },
        equality: {
          attribute: "register_password",
          message: "Пароли не совпадают"
        }
      },
      register_agreement: {
        presence: {
          message: "Даю согласие"
        }
      },
    };

    $('.u-form-register').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,registerConfig,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form__item').find('.u-text-error').removeClass('u-not-valid');
          form.find('.is-form-agreement').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"]').parents('.u-form__item').find('.u-text-error').addClass('u-not-valid').text(el);
            form.find('input[name="' + index + '"]').parents('.is-form-agreement').addClass('u-not-valid');
          });
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').blur();
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);

              $.magnificPopup.close();
            }, 2000);
          }
    });

  /*
  * Корзина
  **/
    var newBuyerConfig = {
      cart_checkout_name: {
        presence: {
          allowEmpty: false,
          message: "Введите имя"
        }
      },
      cart_checkout_surname: {
        presence: {
          allowEmpty: false,
          message: "Введите Фамилию"
        }
      },
      cart_checkout_email: {
        presence: {
          message: "Введите эл. почту"
        },
        email: {
          message: "Введите корректную эл. почту"
        }
      },
      cart_payment: {
        presence: {
          allowEmpty: false,
          message: "Выберите оплату"
        }
      },
      cart_agreement: {
        presence: {
          allowEmpty: false,
          message: "Даю согласие"
        }
      },
    };

    var regularCustomerConfig = {
      cart_checkout_email_regular: {
        presence: {
          message: "Введите эл. почту"
        },
        email: {
          message: "Введите корректную эл. почту"
        }
      },
      cart_checkout_password_regular: {
        presence: {
          allowEmpty: false,
          message: "Введите пароль"
        }
      },
      cart_payment: {
        presence: {
          allowEmpty: false,
          message: "Выберите оплату"
        }
      },
      cart_agreement: {
        presence: {
          allowEmpty: false,
          message: "Даю согласие"
        }
      },
    };

    var setting = newBuyerConfig;

    $(document).on('click', '.u-radio__test__label.is-active', function() {

      if ($(this).attr('data-tab-link') == 'regular_customer') {

          setting = regularCustomerConfig;
      }

      else {
          setting = newBuyerConfig;
      }

      console.log(setting);
    });

    $('.u-form-checkout').on('submit', function(e){

      var form = $(this),
          input = form.serializeObject();               
          validation = validate(input,setting,{fullMessages: false}) || [];
      
          form.removeClass('is-wait');
          form.find('.u-input-style').removeClass('u-not-valid');
          form.find('.u-input-style').parents('.u-form__item').find('.u-text-error').removeClass('u-not-valid');
          form.find('.u-form-agreement').removeClass('u-not-valid');
          
          $.each(validation, function(index, el) {

            form.find('input[name="' + index + '"]').addClass('u-not-valid');
            form.find('input[name="' + index + '"]').parents('.u-form__item').find('.u-text-error').addClass('u-not-valid').text(el);
            form.find('input[name="' + index + '"]').parents('.u-form-agreement').addClass('u-not-valid');
          });
        
          if(validation.length !== 0) {
            e.preventDefault();

          }else{
            e.preventDefault();

            var data = form.serializeFormJSON();
            console.log(data);

            form.addClass('is-wait');
            form.find('.u-btn-submit').attr('disabled', true);
            form.find('.u-input-style').parents('.u-form-item').find('.u-text-error').removeClass('u-not-valid');

            setTimeout(function() {
              form.find('.u-input-style').blur().val('');              
              form.removeClass('is-wait');
              form.find('.u-btn-submit').attr('disabled', false);

              //Ваш заказ успешно оформлен
              popup({
                el: 'ajax/popup/popup_order_success.php'
              });
            }, 2000);
          }
    });
}