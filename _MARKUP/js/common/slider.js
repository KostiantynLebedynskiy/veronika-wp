function carouselSlider(argument) {

  var el = argument.el;

      $(el).slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        swipe: false,
        arrows: true,
        dots: false,
        autoplay: false,
        autoplaySpeed: 4000,
        speed: 300,
        centerMode: false,
        centerPadding: '0',
        pauseOnFocus: false,
        pauseOnHover: false,
        mobileFirst: false,
        adaptiveHeight: false,
        zIndex: 1,
        prevArrow:  '<span class="slick-arrow slick-prev">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M0.3,8.7l7,7C7.5,15.9,7.7,16,8,16c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1.1,0-1.5L3.5,9H19c0.6,0,1-0.5,1-1c0-0.6-0.5-1-1-1H3.5l5.2-5.2c0.4-0.4,0.4-1.1,0-1.5c-0.4-0.4-1.1-0.4-1.5,0l-7,7C-0.1,7.7-0.1,8.3,0.3,8.7z"/>'+
                      '</svg>'+
                    '</span>',
        nextArrow:  '<span class="slick-arrow slick-next">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M19.7,7.3l-7-7c-0.4-0.4-1.1-0.4-1.5,0c-0.4,0.4-0.4,1.1,0,1.5L16.5,7H1C0.5,7,0,7.4,0,8c0,0.6,0.5,1,1,1h15.5l-5.2,5.2c-0.4,0.4-0.4,1.1,0,1.5c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l7-7C20.1,8.3,20.1,7.7,19.7,7.3z"/>'+
                      '</svg>'+
                    '</span>',
        responsive: [
          {
            breakpoint: 1670,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              infinite: true,
              arrows: false,
              dots: true,
              swipe: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              arrows: false,
              dots: true,
              swipe: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              arrows: false,
              dots: true,
              swipe: true
            }
          }
        ]
      });
}

function videoSlider(argument) {

  var el = argument.el;

      $(el).slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        swipe: false,
        arrows: true,
        dots: false,
        autoplay: false,
        autoplaySpeed: 4000,
        speed: 300,
        variableWidth: true,
        centerMode: true,
        centerPadding: '62px',
        pauseOnFocus: false,
        pauseOnHover: false,
        mobileFirst: false,
        zIndex: 1,
        prevArrow:  '<span class="slick-arrow slick-prev">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M0.3,8.7l7,7C7.5,15.9,7.7,16,8,16c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1.1,0-1.5L3.5,9H19c0.6,0,1-0.5,1-1c0-0.6-0.5-1-1-1H3.5l5.2-5.2c0.4-0.4,0.4-1.1,0-1.5c-0.4-0.4-1.1-0.4-1.5,0l-7,7C-0.1,7.7-0.1,8.3,0.3,8.7z"/>'+
                      '</svg>'+
                    '</span>',
        nextArrow:  '<span class="slick-arrow slick-next">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M19.7,7.3l-7-7c-0.4-0.4-1.1-0.4-1.5,0c-0.4,0.4-0.4,1.1,0,1.5L16.5,7H1C0.5,7,0,7.4,0,8c0,0.6,0.5,1,1,1h15.5l-5.2,5.2c-0.4,0.4-0.4,1.1,0,1.5c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l7-7C20.1,8.3,20.1,7.7,19.7,7.3z"/>'+
                      '</svg>'+
                    '</span>',
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              variableWidth: false,
              centerMode: false,
              centerPadding: '0px',
              infinite: true,
              arrows: false,
              dots: true,
              swipe: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              variableWidth: false,
              centerMode: false,
              centerPadding: '0px',
              infinite: true,
              arrows: false,
              dots: true,
              swipe: true
            }
          }
        ]
      });
}

function articlesSlider(argument) {

  var el = argument.el;

      $(el).slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        swipe: false,
        arrows: true,
        dots: true,
        autoplay: false,
        autoplaySpeed: 4000,
        speed: 300,
        centerMode: true,
        variableWidth: true,
        centerPadding: '0',
        pauseOnFocus: false,
        pauseOnHover: false,
        mobileFirst: false,
        adaptiveHeight: false,
        zIndex: 1,
        prevArrow:  '<span class="slick-arrow slick-prev">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M0.3,8.7l7,7C7.5,15.9,7.7,16,8,16c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1.1,0-1.5L3.5,9H19c0.6,0,1-0.5,1-1c0-0.6-0.5-1-1-1H3.5l5.2-5.2c0.4-0.4,0.4-1.1,0-1.5c-0.4-0.4-1.1-0.4-1.5,0l-7,7C-0.1,7.7-0.1,8.3,0.3,8.7z"/>'+
                      '</svg>'+
                    '</span>',
        nextArrow:  '<span class="slick-arrow slick-next">'+
                      '<svg width="20" height="16">'+
                        '<path fill="currentColor" d="M19.7,7.3l-7-7c-0.4-0.4-1.1-0.4-1.5,0c-0.4,0.4-0.4,1.1,0,1.5L16.5,7H1C0.5,7,0,7.4,0,8c0,0.6,0.5,1,1,1h15.5l-5.2,5.2c-0.4,0.4-0.4,1.1,0,1.5c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l7-7C20.1,8.3,20.1,7.7,19.7,7.3z"/>'+
                      '</svg>'+
                    '</span>',
        responsive: [
          {
            breakpoint: 1670,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              arrows: true,
              swipe: true
            }
          },
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              infinite: true,
              arrows: false,
              swipe: true
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              infinite: true,
              arrows: false,
              swipe: true
            }
          }
        ]
      });
}