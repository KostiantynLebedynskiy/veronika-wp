function popup(argument){
  var el = argument.el;

  $.magnificPopup.open({
    type: 'ajax',
    items: {
      src: el
    },
    tLoading: 'Загрузка...',
    closeOnBgClick: true,
    tClose: '',
    removalDelay: 500,
    alignTop: false,
    midClick: true,
    mainClass: 'mfp-move-from-top',
    fixedContentPos: true,
    callbacks: {
      ajaxContentAdded: function() {

      //Validation Forms
        formValidation();

      },
      open: function() {
        $('html').css({
          'margin-right': '0',
          'overflow': 'visible'
        }).addClass('u-popup-mobile');
      },
      close: function() {
        $('html').css({
          'margin-right': '',
          'overflow': ''
        }).removeClass('u-popup-mobile');
      }
    }
  }, 0);
}

function glossaryPopup(argument){

  var el        = argument.el,
      delegate  = argument.delegate;

      el.each(function() {

        $(this).magnificPopup({
          items:{
            src: '.white-popup',
            type: 'inline'
          },
          closeOnContentClick: true,
          // gallery: {
          //   enabled: gallery,
          //   tPrev: '',
          //   tNext: ''
          // },
          fixedContentPos: true,
          callbacks: {
            open: function() {
              console.log($(this)[0].content);
            },
            close: function() {

            },
            buildControls: function() {
              // if (this.items.length > 1) {
              //   this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
              // }
            },
            markupParse: function(template, values, item) {
              console.log(item);
              // optionally apply your own logic - modify "template" element based on data in "values"
              // console.log('Parsing:', template, values, item);
            }
          }
        });
      });
}

$(".c-glossary__all__list2").magnificPopup({
  closeOnBgClick: true,
  closeBtnInside: false,
  preloader: false,
  midClick: true,
  removalDelay: 300,
  overflowY: 'scroll',
  fixedContentPos: false,
  gallery:{
    enabled: true,
    navigateByImgClick: false,
    arrowMarkup: '<button type="button" class="mfp-custom-arrow mfp-arrow-%dir%">'+
                      '<svg aria-hidden="true" width="20" height="16">'+
                        '<path fill="currentColor" d="M0.3,8.7l7,7C7.5,15.9,7.7,16,8,16c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1.1,0-1.5L3.5,9H19c0.6,0,1-0.5,1-1c0-0.6-0.5-1-1-1H3.5l5.2-5.2c0.4-0.4,0.4-1.1,0-1.5c-0.4-0.4-1.1-0.4-1.5,0l-7,7C-0.1,7.7-0.1,8.3,0.3,8.7z"/>'+
                      '</svg>'+
                  '</button>',          
  },
  delegate: '.tips-item',
  type: 'image',
  image: {
    markup:'<div class="b-popup b-popup-glossary__word mfp-with-anim">'+
              '<div class="u-form__head">'+
                '<div class="u-form-title mfp-title">Психотерапия</div>'+
                '<div class="mfp-img" style="display: none"></div>'+
              '</div>'+
              '<div class="u-wd-500">'+
                '<div class="u-glossary__word__section">'+
                  '<p class="mfp-description">Психотерапия (греч. psyche – душа, therapeia – лечение) – система лечения пациента через воздействие на его психику. Сеансы проводятся под руководством специалиста, который в процессе бесед устанавливает доверительные отношения с пациентом, а также практикует применение различных методик.</p>'+
                  '<div class="u-glossary__word__btn">'+                      
                    '<ul>'+
                      '<li>'+
                        '<a href="#" title="">'+
                          '<svg aria-hidden="true" width="20" height="16">'+
                            '<path fill="currentColor" d="M0.3,8.7l7,7C7.5,15.9,7.7,16,8,16c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1.1,0-1.5L3.5,9H19c0.6,0,1-0.5,1-1c0-0.6-0.5-1-1-1H3.5l5.2-5.2c0.4-0.4,0.4-1.1,0-1.5c-0.4-0.4-1.1-0.4-1.5,0l-7,7C-0.1,7.7-0.1,8.3,0.3,8.7z"/>'+
                          '</svg>'+
                          '<span>Психостимуляторы</span>'+
                        '</a>'+
                      '</li>'+
                      '<li>'+
                        '<a href="#" title="">'+
                          '<svg aria-hidden="true" width="20" height="16">'+
                            '<path fill="currentColor" d="M19.7,7.3l-7-7c-0.4-0.4-1.1-0.4-1.5,0c-0.4,0.4-0.4,1.1,0,1.5L16.5,7H1C0.5,7,0,7.4,0,8c0,0.6,0.5,1,1,1h15.5l-5.2,5.2c-0.4,0.4-0.4,1.1,0,1.5c0.2,0.2,0.5,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3l7-7C20.1,8.3,20.1,7.7,19.7,7.3z"/>'+
                          '</svg>'+
                          '<span>Паническая атака</span>'+
                        '</a>'+
                      '</li>'+
                    '</ul>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>',
    titleSrc: function(item) {
      //console.log(item.el)
      return item.el.text();
    }
  },
  callbacks: {
    buildControls: function() {
      this.contentContainer.append(this.arrowLeft.add(this.arrowRight));
    },
    beforeOpen: function() {
      //console.log(this.items);
      //this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
      //this.st.mainClass = this.st.el.attr('data-effect');
    },
    markupParse: function(template, values, item){
        values.description = item.el.data('description');
    },
    imageLoadComplete: function() {
        $(".mfp-custom-arrow").css("opacity",1);
     },
     beforeClose: function() {
       $(".mfp-custom-arrow").css("opacity",0); 
    },
    open: function() {
        var mfp = $.magnificPopup.instance;
        var proto = $.magnificPopup.proto;
        console.log(1);

        // extend function that moves to next item
        mfp.next = function() {

            // if index is not last, call parent method
            if(mfp.index < mfp.items.length - 1) {
                proto.next.call(mfp);
                console.log(2);
            } else {
              console.log(3);
               // otherwise do whatever you want, e.g. hide "next" arrow
            }
        };

        // same with prev method
        mfp.prev = function() {
            if(mfp.index > 0) {
                proto.prev.call(mfp);
                console.log(4);
            }
        };

    }
  }
});