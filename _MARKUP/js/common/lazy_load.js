function imgCreateOnscroll(sliderImageBlock) {
  var sliderImageSrc = sliderImageBlock.attr('data-imagesrc');
  var sliderImage = document.createElement('img');
  sliderImage.src = sliderImageSrc;
  sliderImage.style.opacity = 0;
  sliderImage.alt = sliderImageBlock.attr('data-alt');
  sliderImage.onload = function() {
    sliderImageBlock.addClass('img-preloaded').prepend(sliderImage);
    $(sliderImage).animate({
      opacity: 1
    }, 500);
  }
}

function lazyLoadItems() {
  $('.is-lazy-load .u-lazy:not(.waypoint-inited)').each(function() {
      $(this).addClass('waypoint-inited').waypoint({
        handler: function(direction) {
          $(this.element).each(function() {
            imgCreateOnscroll($(this));
          });
          this.destroy()
        },
        offset: '90%'
      });
  });
}