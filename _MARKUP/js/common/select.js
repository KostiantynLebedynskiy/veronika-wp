function selectInit(argument) {

	var element      	= argument.element,
			dropdownClass = argument.dropdownClass,
			search        = argument.search;

		  $(element).select2({
        width: "100%",
        minimumResultsForSearch: search,
        dropdownCssClass: dropdownClass
      });
}