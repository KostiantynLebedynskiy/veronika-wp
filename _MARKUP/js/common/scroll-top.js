(function() {
    "use strict";

    var docElem = document.documentElement,
        didScroll = false,
        changeHeaderOn = 550;
        document.querySelector( '.u-btn-scroll-top' );
    function init() {
        window.addEventListener( 'scroll', function() {
            if( !didScroll ) {
                didScroll = true;
                setTimeout( scrollPage, 50 );
            }
        }, false );
    }
    
})();

$(window).scroll(function(event){
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        $(".u-btn-scroll-top").addClass("is-show");
    } else {
        $(".u-btn-scroll-top").removeClass("is-show");
    }
});

$('.u-btn-scroll-top').on('click',function(){
    $('html, body').animate({scrollTop: 0}, 'slow');
    return false;
});