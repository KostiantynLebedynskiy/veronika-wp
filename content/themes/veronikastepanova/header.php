<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title><?php bloginfo('name'); ?> | <?php is_home() || is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
  <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
  <!-- stylesheet -->
  
  <!-- end stylesheet -->
  <!--[if IE]>
      <link rel="stylesheet" type="text/css" href="css/all-ie-only.css" />
  <![endif]-->
  <?php wp_head(); ?> 
</head>
<body>
