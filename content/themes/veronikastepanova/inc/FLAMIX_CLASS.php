<?php
/* ==========================================================================
   FLAMIX | Клас с набором необходимых методов
   ========================================================================== */
class FLAMIX {
	/* ==========================================================================
	   Количество просмотров
	   ========================================================================== */
	public static function getPostViews($postID){     
	    $count_key = 'post_views_count';     
	    $count = get_post_meta($postID, $count_key, true);     
	        if($count=='') {         
	            delete_post_meta($postID, $count_key);         
	            add_post_meta($postID, $count_key, '0');         
	            return "0";     
	        }     
	    return $count; 
	} 
	public static function setPostViews($postID) {     
	    $count_key = 'post_views_count';     
	    $count = get_post_meta($postID, $count_key, true);     
	    if($count=='') {         
	        $count = 0;         
	        delete_post_meta($postID, $count_key);         
	        add_post_meta($postID, $count_key, '0');     
	    }
	    else {         
	        $count++;         
	        update_post_meta($postID, $count_key, $count);     
	    } 
	} 
	/* ==========================================================================
	   пагинация
	   ========================================================================== */
	public static function wp_corenavi() {
	  global $wp_query;
	  $pages = '';
	  $max = $wp_query->max_num_pages;
	  if (!$current = get_query_var('paged')) $current = 1;
	  $a['base'] = str_replace(999999999, '%#%', get_pagenum_link(999999999));
	  $a['total'] = $max;
	  $a['current'] = $current;

	  $total = 0; //1 - выводить текст "Страница N из N", 0 - не выводить
	  $a['mid_size'] = 5; //сколько ссылок показывать слева и справа от текущей
	  $a['end_size'] = 3; //сколько ссылок показывать в начале и в конце
	  $a['prev_text'] = ''; //текст ссылки "Предыдущая страница"
	  $a['next_text'] = ''; //текст ссылки "Следующая страница"

	  if ($max > 1) echo '<div class="b-pagination"><div class="container"><div class="wrapper-pag">';
	  if ($total == 1 && $max > 1) $pages = '<span class="page">Страница ' . $current . ' из ' . $max . '</span>'."\r\n";
	  echo $pages . paginate_links($a);
	  if ($max > 1) echo '</div></div></div>';
	}
	/* ==========================================================================
	   Синтаксис слов
	   ========================================================================== */
	public static function syntax_product($number) {
	    if ($number == 0) {
	        $output = 'нет товаров';
	    } elseif ($number == 1) {
	        $output = ' товар';
	    } elseif (($number > 20) && (($number % 10) == 1)) {
	        $output = ' товар';
	    } elseif ((($number >= 2) && ($number <= 4)) || ((($number % 10) >= 2) && (($number % 10) <= 4)) && ($number > 20)) {
	        $output = ' товара';
	    } else {
	        $output = ' товаров';
	    }
	    return $output;
	}
	function get_first_letter( $str ) {
		return mb_substr($str, 0, 1, 'utf-8');
	}
	function get_post_letters($posts) {
		$post_arr = array();
		foreach( $posts as $k => $post ) :
			$fl = self::get_first_letter( $post->post_title );
			$prev_fl = isset( $posts[ ($k-1) ] ) ? self::get_first_letter( $posts[ ($k-1) ]->post_title ) : '';
			if( $prev_fl != $fl ) 
				array_push($post_arr, $fl);
		endforeach;
		return $post_arr;
	}
/* ==========================================================================
	clientId
========================================================================== */
	function gaParseCookie() {
	  if (isset($_COOKIE['_ga'])) {
	    list($version,$domainDepth, $cid1, $cid2) = preg_split('[\.]', $_COOKIE["_ga"],4);
	    $contents = array('version' => $version, 'domainDepth' => $domainDepth, 'cid' => $cid1.'.'.$cid2);
	    $cid = $contents['cid'];
	  }
	  else $cid = gaGenUUID();
	  return $cid;
	}

}