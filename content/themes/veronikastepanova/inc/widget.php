<?php 
/* ==========================================================================
   FLAMIX | Stock
   ========================================================================== */

class FLAMIX_Stock extends WP_Widget {
     public function __construct() {
           parent::__construct(
                 'widget_WP_stock',
                 'FLAMIX | STOCK FOR CUSTOMER SUCCESS STORIES',
                 array( 'description' => __( 'Вставка виджета', 'text_domain' ), )
           );
     }
     public function update( $new_instance ) {
			$instance = array();
           	$instance['post_url'] = $new_instance['post_url'];
           	$instance['image_stock'] = $new_instance['image_stock'];

			return $instance;
     }
     public function form( $instance ) {
    ?>
      	<div class="wrapp_bl widget_bl">
  			<label for="<?php echo $this->get_field_id( 'image_stock' ); ?>"><?php _e('DOWNLOAD IMAGE:', 'example'); ?></label>
            <img id="meta-image-url"  src="<?php echo $instance['image_stock']; ?>" alt="" style="max-width:100%;" />
           <input type="hidden" id="meta-image" name="<?php echo $this->get_field_name( 'image_stock' ); ?>" style="width:100%;" value="<?php echo $instance['image_stock']; ?>" />
        <div class="nav-bottom">
          <input type="button" id="meta-image-button" class="button" value="Upload" />
          <input type="button" id="meta-image-button-delete" class="button" value="Delete" />
        </div>
      </div>
     	<p>
  			<label for="<?php echo $this->get_field_id( 'post_url' ); ?>"><?php _e('CUSTOMER SUCCESS STORIES POST', 'example'); ?></label>
        <select class="select-menu" name="<?php echo $this->get_field_name( 'post_url' ); ?>"  value="<?php echo $instance['post_url']; ?>">
          <?php
          global $post;
            $posts = get_posts( array(
              'numberposts'     => -1,
              'post_type'       => 'success_stories'
            ) );
            foreach($posts as $post) : setup_postdata($post);
          ?>
          <option value="<?=$post->ID; ?>" <?php if($instance['post_url'] == $post->ID) echo 'selected'; ?> ><?php the_title(); ?></option>
          <?php endforeach; ?>
        </select>
  		</p>
		
    <?php
     }
     public function widget( $args, $instance ) {
	 extract( $args );

  	 $post_url = $new_instance['post_url'];
  	 $post_url = isset( $instance['post_url'] ) ? $instance['post_url'] : false;
  	 $image_stock = $new_instance['image_stock'];
  	 $image_stock = isset( $instance['image_stock'] ) ? $instance['image_stock'] : false;

    /*Post data*/
    $post_cont = get_post( $post_url );
    $story_wrap_term = get_field('success_stories_page', $post_url);
?>  
<div class="akcia-widget">
  <div class="col-md-12 col-sm-12">
    <div class="col-md-6 col-sm-6 hidden-xs item">
      <div class="akcia-widget-img" style="background-image: url(<?=$image_stock; ?>);"></div>
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 item">
      <div class="akcia-widget-text">
        <div class="akcia-widget-logotype">
          <?=wp_get_attachment_image( $story_wrap_term[0]['download_logo']['id'], array(0, 35)); ?>
        </div>
        <div class="akcia-widget-suptitle"><?=$story_wrap_term[0]['of_the_title']; ?></div>
        <h3 class="akcia-widget-title"><?=$post_cont->post_title?></h3>
        <div class="akcia-widget-descr">
          <p><?=$story_wrap_term[0]['subtitle']; ?></p>
        </div>
        <a href="<?=get_the_permalink($post_url); ?>" class="button">Read story</a>    
      </div>
    </div>
  </div>
</div> 
	
<?php
     }
}


/* ==========================================================================
   FLAMIX | Banner
   ========================================================================== */

class FLAMIX_ban_catalog extends WP_Widget {
     public function __construct() {
           parent::__construct(
                 'widget_WP_post',
                 'FLAMIX | Баннер для страниц каталога',
                 array( 'description' => __( 'Вставка виджета', 'text_domain' ), )
           );
     }
     public function update( $new_instance, $old_instance ) {
      $instance = array();
            $instance['link_img'] = $new_instance['link_img'];
            $instance['sale'] = $new_instance['sale'];
            $instance['descript'] = $new_instance['descript'];
            $instance['duration'] = $new_instance['duration'];

      return $instance;
     }
     public function form( $instance ) {
    ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'link_img' ); ?>"><?php _e('Ссылка на картинку:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'link_img' ); ?>" name="<?php echo $this->get_field_name( 'link_img' ); ?>" style="width:100%;" value="<?php echo $instance['link_img']; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'sale' ); ?>"><?php _e('Скидка:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'sale' ); ?>" name="<?php echo $this->get_field_name( 'sale' ); ?>" style="width:100%;" value="<?php echo $instance['sale']; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'descript' ); ?>"><?php _e('Описания:', 'example'); ?></label>
      <textarea  id="<?php echo $this->get_field_id( 'descript' ); ?>" name="<?php echo $this->get_field_name( 'descript' ); ?>" style="width:100%; min-height: 80px"><?php echo $instance['descript']; ?> </textarea>
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'duration' ); ?>"><?php _e('Длительность акции:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'duration' ); ?>" name="<?php echo $this->get_field_name( 'duration' ); ?>" style="width:100%;" value="<?php echo $instance['duration']; ?>" />
    </p>
    
    
    <?php
     }
     public function widget( $args, $instance ) {
   extract( $args );

     $link_img = $new_instance['link_img'];
     $link_img = isset( $instance['link_img'] ) ? $instance['link_img'] : false;
     $sale = $new_instance['sale'];
     $sale = isset( $instance['sale'] ) ? $instance['sale'] : false;
     $descript = $new_instance['descript'];
     $descript = isset( $instance['descript'] ) ? $instance['descript'] : false;
     $duration = $new_instance['duration'];
     $duration = isset( $instance['duration'] ) ? $instance['duration'] : false;
?>  
  <div class="s-sect akcia-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="promo-akcia table">
            <div class="akcia-image" style="background-image: url(<?=$link_img;?>);">
              <span class="akcia-discount">
                <?=$sale;?>
              </span>
            </div>
            <div class="akcia-text">
              <div class="akcia-text-title">
                <?=$descript;?>
              </div>
              <div class="akcia-termin">
                <?=$duration;?>
              </div>
              <div class="close-akcia"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
<?php
     }
}


/* ==========================================================================
   FLAMIX | Social icons
   ========================================================================== */

class FLAMIX_social_network extends WP_Widget {
     public function __construct() {
           parent::__construct(
                 'widget_WP_social',
                 'FLAMIX | Социальные сети',
                 array( 'description' => __( 'Вставка виджета', 'text_domain' ), )
           );
     }
     public function update( $new_instance, $old_instance ) {
      $instance = array();
            $instance['facebook'] = $new_instance['facebook'];
            $instance['vk'] = $new_instance['vk'];
            $instance['odnoklassniki'] = $new_instance['odnoklassniki'];

      return $instance;
     }
     public function form( $instance ) {
    ?>
      <p>
      <label for="<?php echo $this->get_field_id( 'facebook' ); ?>"><?php _e('Ссылка на facebook:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'facebook' ); ?>" name="<?php echo $this->get_field_name( 'facebook' ); ?>" style="width:100%;" value="<?php echo $instance['facebook']; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'vk' ); ?>"><?php _e('Ссылка на ВКонтакте:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'vk' ); ?>" name="<?php echo $this->get_field_name( 'vk' ); ?>" style="width:100%;" value="<?php echo $instance['vk']; ?>" />
    </p>
    <p>
      <label for="<?php echo $this->get_field_id( 'odnoklassniki' ); ?>"><?php _e('Ссылка на Одноклассники:', 'example'); ?></label>
      <input type="text" id="<?php echo $this->get_field_id( 'odnoklassniki' ); ?>" name="<?php echo $this->get_field_name( 'odnoklassniki' ); ?>" style="width:100%;" value="<?php echo $instance['odnoklassniki']; ?>" />
    </p>
  
    <?php
     }
     public function widget( $args, $instance ) {
   extract( $args );

     $facebook = $new_instance['facebook'];
     $facebook = isset( $instance['facebook'] ) ? $instance['facebook'] : false;
     $vk = $new_instance['vk'];
     $vk = isset( $instance['vk'] ) ? $instance['vk'] : false;
     $odnoklassniki = $new_instance['odnoklassniki'];
     $odnoklassniki = isset( $instance['odnoklassniki'] ) ? $instance['odnoklassniki'] : false;

?>  
  <ul class="social-link list">
    <li><a href="<?= $facebook; ?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
    <li><a href="<?= $vk; ?>" class="vk"><i class="fa fa-vk"></i></a></li>
    <li><a href="<?= $odnoklassniki; ?>" class="odnoklassniki"><i class="fa fa-odnoklassniki"></i></a></li>
  </ul>
  
<?php
     }
}

add_action( 'widgets_init', function(){
  register_widget( 'FLAMIX_social_network' );
  register_widget( 'FLAMIX_ban_catalog' );
  register_widget( 'FLAMIX_Stock' );
     
});