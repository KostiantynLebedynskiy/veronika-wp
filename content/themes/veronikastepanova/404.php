<?php 
/**
 * page 404
 *
 * @package WordPress
 * @subpackage example
 * @since example 1.0
 */

get_header(); ?>

	<h2>Error 404 - Page Not Found</h2>

<?php get_sidebar(); ?>

<?php get_footer(); ?>