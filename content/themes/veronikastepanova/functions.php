<?php 
  require_once  $_SERVER["DOCUMENT_ROOT"] . '/vendor/autoload.php';


/* ==========================================================================
   Thumbnails
   ========================================================================== */

if ( function_exists( 'add_theme_support' ) ) {
  add_theme_support( 'post-thumbnails' ); 
  add_image_size( 'image_config', 150, 150, true );
}
/* ==========================================================================
   Register menus
   ========================================================================== */
register_nav_menus(array(
	'main_menu' => 'Главное меню'
));
/* ==========================================================================
   Add widget
   ========================================================================== */
register_sidebar(array(
    'name' => 'Social', 
    'id' => 'widget-social', 
    'before_widget' => '', 
    'after_widget' => ''
));

/* ==========================================================================
   admin-style
   ========================================================================== */

function load_custom_wp_admin_style() {
        wp_register_style( 'custom_wp_admin_css', get_template_directory_uri() . '/admin/css/admin.css', false, '1.0.0' );
        wp_enqueue_style( 'custom_wp_admin_css' );
       wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css' );
        wp_enqueue_script("admin-js", get_template_directory_uri()."/admin/js/admin.js", array(),'',TRUE);
}
    add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );
    /*Add theme css and js*/
    add_action( 'wp_enqueue_scripts', 'add_external_stylesheets' );
    add_action( 'wp_enqueue_scripts', 'add_external_js' );

    function add_external_stylesheets()
    {
    //styles 
       // wp_enqueue_style( 'example-css', get_template_directory_uri() . '/css/example.css', false, '1.0.0' );
         
   //main style
        wp_enqueue_style("main", get_stylesheet_uri() , false, "1.0");
    }

    function add_external_js()
    {
    //scripts
        wp_enqueue_script("jquery-js", '//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js');
        
    //main script
        wp_enqueue_script("main-js", get_template_directory_uri()."/js/main.js", array(),'',TRUE);
    }
/*Add theme css and js*/

/* ==========================================================================
   Remove p around picture
   ========================================================================== */
function filter_ptags_on_images($content){
//функция preg replace, которая убивает тег p
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
/* ==========================================================================
   Support svg
   ========================================================================== */
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);

/**
 * Hightlight parrent menu item.
 */
function add_current_nav_class($classes, $item, $args) {
      // Getting the current post details
      global $post;
      // Getting the post type of the current post
      $current_post_type = get_post_type_object(get_post_type($post->ID));
      // $current_post_type_slug = $current_post_type->rewrite[slug];
      $current_post_type_slug = $current_post_type->name;
      //if ($current_post_type_slug == 'post') $current_post_type_slug = 'blog';
      // Getting the URL of the menu item
      $menu_slug = strtolower(trim($item->url));
      // If the menu item URL contains the current post types slug add the current-menu-item class
     if ($current_post_type_slug && mb_strpos($menu_slug,$current_post_type_slug) !== false) {
         $classes[] = 'current-menu-item sl-'.$current_post_type_slug;
      }
      $menu_locations = get_nav_menu_locations();
    // Return the corrected set of classes to be added to the menu item
    return $classes;
}
add_action('nav_menu_css_class', 'add_current_nav_class', 10, 3 );


if( is_admin() ){
  // отключим проверку обновлений при любом заходе в админку...
  remove_action( 'admin_init', '_maybe_update_core' );
  remove_action( 'admin_init', '_maybe_update_plugins' );
  remove_action( 'admin_init', '_maybe_update_themes' );

  // отключим проверку обновлений при заходе на специальную страницу в админке...
  remove_action( 'load-plugins.php', 'wp_update_plugins' );
  remove_action( 'load-themes.php', 'wp_update_themes' );

  // оставим принудительную проверку при заходе на страницу обновлений...
  //remove_action( 'load-update-core.php', 'wp_update_plugins' );
  //remove_action( 'load-update-core.php', 'wp_update_themes' );

  // внутренняя страница админки "Update/Install Plugin" или "Update/Install Theme" - оставим не мешает...
  //remove_action( 'load-update.php', 'wp_update_plugins' );
  //remove_action( 'load-update.php', 'wp_update_themes' );

  // событие крона не трогаем, через него будет проверяться наличие обновлений - тут все отлично!
  //remove_action( 'wp_version_check', 'wp_version_check' );
  //remove_action( 'wp_update_plugins', 'wp_update_plugins' );
  //remove_action( 'wp_update_themes', 'wp_update_themes' );

  /**
   * отключим проверку необходимости обновить браузер в консоли - мы всегда юзаем топовые браузеры!
   * эта проверка происходит раз в неделю...
   * @see https://wp-kama.ru/function/wp_check_browser_version
   */
  add_filter( 'pre_site_transient_browser_'. md5( $_SERVER['HTTP_USER_AGENT'] ), '__return_true' );
}